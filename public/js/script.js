// const { default: axios } = require("axios");

// const { get } = require("lodash");


function showarticle(id){
    axios.get('/articolo/'+id).then(Response=>{
        document.getElementById('articles-container').innerHTML = Response.data;
    })
}

function adminfilter(){
    var macrocat = document.getElementById('macroselectfilter').value;
    var cat = document.getElementById('catselectfilter').value;
    var macroid = 0;
    var catid = 0;
    switch(macrocat){
        case 'Tutte':
            macroid = 0;
            break;
        case 'Neonato':
            macroid = 1;
            break;
        case 'Neonata':
            macroid = 2;
            break;
        case 'Bimba':
            macroid = 3;
            break;
        case 'Bimbo':
            macroid = 4;
            break;
        case 'MiniMe':
            macroid = 5;
            break;
    }
    switch(cat){
        case 'Tutte':
            catid = 0;
            break;
        case 'Accessori':
            catid = 1;
            break;
        case 'T-Shirt e Magliette':
            catid = 2;
            break;
        case 'Felpe e Maglioni':
            catid = 3;
            break;
        case 'Gonne':
            catid = 4;
            break;
        case 'Pantaloni':
            catid = 5;
            break;
        case 'Abiti':
            catid = 6;
            break;
        case 'Bluse e Camicie':
            catid = 7;
            break;
        case 'Giacche e Cardigan':
            catid = 8;
            break;
        case 'Giubbini e Capispalla':
            catid = 9;
            break;
    }
    axios.get('/admin/articoli/'+macroid+'/'+catid).then(Response=>{
        document.getElementById('articles-container').innerHTML = Response.data;
    })
}

function adminfiltrotutti(prova){
    axios.get('/admin/articoli/tutti/'+prova).then(Response=>{
        document.getElementById('articles-container').innerHTML = Response.data;
    })
}

function deletearticle(id){
    axios.post('/admin/articoli/delete/'+id).then(Response=>{
        document.getElementById('articles-container').innerHTML = Response.data;
    })
}

function modifica(hello){
    axios.post('/admin/articoli/modifica/'+hello).then(Response=>{
        alert(Response.data);
    })
}

function addsize(id){
    var sizeid = document.getElementById('sizetobeadded').value;
    var qta = document.getElementsByName('qta-addsize')[0].value;
    axios.post('/admin/articoli/addsize/'+id+'/'+sizeid+'/'+qta).then(Response=>{
        document.getElementById('articles-container').innerHTML = Response.data;
    })
}

function removeimg(pid, aid){
    axios.post('/admin/articoli/removeimg/'+pid+'/'+aid).then(Response=>{
        document.getElementById('articles-container').innerHTML = Response.data;
    })
}

function opensidebar(){
    $('#sidebarCloser').removeClass('rotate2');
    $('#sidebarCloser').addClass('rotate');
    $('.sidebar').css('right', '0');
}

function closesidebar(){
    $('#sidebarCloser').removeClass('rotate');
    $('#sidebarCloser').addClass('rotate2');
    $('.sidebar').css('right', '-100vw');
}

function search(s){
    if(s == '' || s.length <= 2){
        $('#risultato-ricerca').addClass('d-none');
    }
    else if(s.length > 2){
        $.ajax({
            method: 'GET',
            data: {
                s: s
            },
            url: '/home/search'
        }).done(function(response){
            var result = JSON.parse(response);
            if(result != '' && result != undefined){
                for(let i = 0; i < result.articoli.length; i++){
                    if($('#ricerca-'+result.articoli[i].id).length > 0){
                    }
                    else{
                        var articlecontainer = document.createElement('div');
                        var articleimgdetail = document.createElement('div');
                        var articleimg = document.createElement('img');
                        $(articlecontainer).attr('id', 'ricerca-'+result.articoli[i].id);
                        $(articlecontainer).addClass('p-3 mb-3 bg-white radius-10');
                        $(articleimgdetail).addClass('d-flex');
                        $(articleimg).addClass('img-fluid mr-3 radius-10').attr('src', result.immagini[i]).css('max-width', '35%');
                        var articledetail = document.createElement('div');
                        $(articledetail).attr('id', 'detail-container').addClass('w-100 d-flex flex-column justify-content-between');
                        var detail = document.createElement('div');
                        $(detail).addClass('mb-0')
                            .append('<p class="text-truncate mb-1 font-weight-medium text-primary">'+result.articoli[i].name+'</p>')
                            .append('<p class="font-weight-medium text-primary">€ '+result.articoli[i].price+',00</p>');
                        $(articledetail).append(detail);
                        var detailbuttoncontainer = document.createElement('div');
                        $(detailbuttoncontainer).addClass('w-100 text-right').append('<a href="'+window.location.origin+'/article?id='+btoa(result.articoli[i].id+'_'+result.articoli[i].name)+'">Visualizza <i aria-hidden="true" class="fas fa-arrow-right fa-fw"></i></a>');
                        $(articledetail).append(detail);
                        $(articledetail).append(detailbuttoncontainer);
                        $(articleimgdetail).append(articleimg);
                        $(articleimgdetail).append(articledetail);
                        $('#risultato-ricerca').append(articlecontainer);
                        $(articlecontainer).append(articleimgdetail);
                    }
                }
                $('#risultato-ricerca').removeClass('d-none');                        
            }
        });
    }
}

function stopsearch(el){
    $('#risultato-ricerca').html('');
    $('#risultato-ricerca').addClass('d-none');
    $('#search-input').val('');
}

$(document).click(function(e){
    if($('#risultato-ricerca').length > 0 && !$('#risultato-ricerca').hasClass('d-none')){
        if($('#risultato-ricerca').find(e.target) || $('#risultato-ricerca') == e.target){
        }
        else{
            $('#risultato-ricerca').addClass('d-none');
        }
    }
});

function usermenu(){
    if($('#cart-menu').length > 0){
        if($('#cart-menu').hasClass('d-none')){
        }
        else{
            showcart();
        }
        $('#user-menu').toggleClass('d-flex');
        if($('#user-menu').hasClass('d-flex')){
            $("#user-menu").animate({height: 450}, 200 );
        }
        else{
            $("#user-menu").animate({height: 0}, 200 );
        }
    }
    else{
        $('#user-menu').toggleClass('d-flex');
        $('#user-menu').toggleClass('d-none');
        $('#user-menu').toggleClass('guest-menu');
    }
}

// function showarticleview(id){
//     $('#articleViewContainer').html('');
//     axios.get('/showarticle/'+id).then(Response=>{
//         var view = Response.data;
//         var div = document.createElement('div');
//         div.innerHTML = view;
//         $('#articleViewContainer').html(div);
//     });

//     articleslide();
// }


function addtocart(aid, sizeid, qta){
    var cond = false;
    if($('#taglia :selected').attr('value').length > 0){
        cond = true;
    }else{
        $('#alert-taglia').removeClass('d-none');
        cond = false;
    }
    if($('#quantità').val().length > 0){
        cond = true;
    }
    else{
        $('#alert-qta').removeClass('d-none');
        cond = false;
    }
    if(cond == true){
        axios.post('/carrello/add/'+aid+'/'+sizeid+'/'+qta).then(Response=>{
            // $('#itemsInCart').html(Response.data);
            // $('#itemsInCartMobile').html(Response.data);

            $('#cart-menu').html(Response.data);
            var itemsincart = $('#itemsInCart').html();
            $('#itemsInCart').html(parseInt(itemsincart)+parseInt(qta));
            if($('#itemsInCart').hasClass('d-none')){
                $('#itemsInCart').removeClass('d-none');
            }
        });
        $('#cart-menu').addClass('cart-scroll');
    }
}

function showcart(){
    if($('#user-menu').hasClass('d-flex')){
       usermenu();
    }
    else{
    }
    $('#cart-menu').toggleClass('d-none');
    if($('#cart-menu').hasClass('d-none')){
        $("#cart-menu").css('height', '0');
    }
    else{
        $("#cart-menu").animate({height: 450}, 200 );
    }
}

function changeqta(op, cid, elements){
    axios.post('/carrello/changeqta/'+op+'/'+cid).then(Response=>{
        $('#cart-menu').html(Response.data);
        if(op == 'sum'){
            var val = parseInt($('#itemsInCart').html());
            $('#itemsInCart').html(val+1);
        }
        else if(op == 'dif'){
            var val = parseInt($('#itemsInCart').html());
            if(val > elements){
                $('#itemsInCart').html(val-1);
            }
        }
    });
}

function removeFromCart(cid, cqta){
    axios.post('/carrello/remove/'+cid).then(Response=>{
        $('#cart-menu').html(Response.data);
        var val = parseInt($('#itemsInCart').html());
        $('#itemsInCart').html(val-cqta);
        if($('#itemsInCart').html() == 0){
            $('#cart-menu').removeClass('cart-scroll');
        }
    });
}

function shopfilter(){
    var macrocatid = $('#macrocatfilter').val();
    var catid = $('#catfilter').val();
    var collectionid = $('#collectionfilter').val();
    var sortid = $('#sortfilter').val();
    
    $.ajax({
        method: "GET",
        url: "/shopfilter",
        data: {
            macrocatid: macrocatid,
            catid: catid,
            collectionid: collectionid,
            sortid: sortid,
        },
        beforeSend: function(){
            $('.spinner-container').removeClass('d-none').addClass('d-flex');
        }
    }).done(function(response){
        $('#articles-list').html(response);
        $('.spinner-container').removeClass('d-flex').addClass('d-none');
        $('#pageurlmacrocat').text($('#macrocatfilter option:selected').text());
    });
}

// function usertab(link, tab){
//     $('.user-tablink').removeClass('active');
//     $('.user-tab').removeClass('active');
//     $('.user-tab').addClass('d-none');
//     $(link).addClass('active');
//     $(tab).addClass('active');
//     $(tab).removeClass('d-none');
// }

//NON SO SE LA VUOI SISTEMARE DOPO QUESTA FUNZIONE, L'HO ARRANGIATA IN 5 MINUTI, TI POSSONO BYPASSARE IL DISABLE SUL SUBMIT DEL TASTO DALL'ISPEZIONA ELEMENTO PERO DI BASE FUNZIONA PER CHI NON SA COSA SIA L'ISPEZIONE, SORT D TRMO. 
//SONO LE 00:18, TRA POCO VADO A DORMIRE; SPERO CHE TI PIACCIA IL LAVORO
//PER FAVORE VOGLIO SOLO CHE TU STIA BENE NON FARMI PREOCCUPARE, TI VOGLIO BENE, TANTO.
function checkPass() {
    if ($('#password').val() == $('#confirm_password').val()) {
        $('#message').html('Matching').css('color', 'green');
        $('#submitPassButton').removeClass('disabled');
    } else {
        $('#message').html('Not Matching').css('color', 'red');
        $('#submitPassButton').addClass('disabled');
    }
}

// function submit() {
//     let newsLetter = $('#newsMail').val();
//     axios.post('/home/'+ newsLetter).then((Response) =>{
//         console.log(Response.data)
//     });
// }

