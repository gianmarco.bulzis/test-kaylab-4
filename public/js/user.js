// const { default: axios } = require("axios");

function favourite(uid, aid, el = null){
    var heart = $(el).find('i');
    axios.post('/favourite', {userid: uid, articleid: aid}).then(Response=>{
        // Swal.fire({
        //     title: 'Success!',
        //     text: 'Articolo aggiunto ai preferiti',
        //     icon: 'success',
        //     confirmButtonText: 'Cool',
        //     toast: true
        // });

        if(Response.data == 'added'){
            $(heart).removeClass('fal').addClass('fas');
        }
        else if(Response.data == 'deleted'){
            $(heart).removeClass('fas').addClass('fal');
        }
    });
}

$(function(){
    if($('section.profile-section').length > 0){
        $('#navbar #cartMenuToggler').addClass('d-none').removeClass('mx-2');
        $('#navbar #itemsInCart').addClass('d-none');
        $('#navbar #user-navlink').addClass('mr-0');
    }
})