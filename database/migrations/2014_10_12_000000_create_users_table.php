<?php

use App\Models\User;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('name')->nullable();
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->string('cognome')->nullable();
            $table->string('sesso')->nullable();
            $table->date('birthdate')->nullable();
            $table->string('telefono')->nullable();
            $table->rememberToken();
            $table->timestamps();
            // $table->integer('daysleft')->default(30);
        });

        $admin = new User();
        $admin->name = "admin";
        $admin->email = "admin@admin.it";
        $admin->password = bcrypt('adminadmin');
        $admin->save();

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}