<?php

use App\Models\Size;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSizesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sizes', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->timestamps();
        });

        $sizes = ['xs', 's', 'm', 'l', 'xl', 'xxl', '0/3-mesi', '3/6-mesi', '6/9-mesi', '9/12-mesi', '12/18-mesi', '18/24-mesi', '2/3-anni', '3/4-anni', '4/5-anni', '5/6-anni', '6/7-anni', '7/8-anni', '9/10-anni', '11/12-anni', '0/2-anni', '2/4-anni', '4/6-anni'];
        foreach($sizes as $size){
            $s = new Size();
            $s->name = $size;
            $s->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sizes');
    }
}
