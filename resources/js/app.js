/* Custom Js */
require('./alert_fade');
require('./navbar-scroll');

/* Bootstrap Js */
require('./bootstrap');

/* Slick-Carousel */
require('slick-carousel');

/* Custom JS */

/* Vue */
window.Vue = require('vue').default;
Vue.component('example-component', require('./components/ExampleComponent.vue').default);
const app = new Vue({
    el: '#app',
});



