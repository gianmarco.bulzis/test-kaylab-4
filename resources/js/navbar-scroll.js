$(function(){
    $(window).scroll(navbarscroll);

    function navbarscroll(){
        if($(document).scrollTop() > 180){
            $('#macrocat-nav').addClass('shadow');
            $('#macrocat-nav').css('shadow');
            $('#macrocat-nav').addClass('sticky-top');
        }
        else{
            $('#macrocat-nav').removeClass('shadow');
            $('#macrocat-nav').removeClass('sticky-top');
        }
    }
});