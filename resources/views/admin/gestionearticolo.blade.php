<div class="container border border-primary shadow bg-dark text-white" style="overflow: hidden; border-radius: 25px;" id="gestionearticolo">
    <div class="row">
        <div class="col-12 text-center mt-3">
            <h1>Gestione Articolo</h1>
        </div>
        <div class="col-12 text-center">

{{-- CAROSELLO --}}

            <div id="carouselExampleIndicators" class="carousel slide" data-interval="false">
                <ol class="carousel-indicators mt-5">
                    @if($articolo->pictures->count() > 0)
                    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                    @endif
                    @if($articolo->pictures->count() > 1)
                    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                    @endif
                    @if($articolo->pictures->count() > 2)
                    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                    @endif
                    @if($articolo->pictures->count() > 3)
                    <li data-target="#carouselExampleIndicators" data-slide-to="3"></li>
                    @endif
                    @if($articolo->pictures->count() > 4)
                    <li data-target="#carouselExampleIndicators" data-slide-to="4"></li>
                    @endif
                    @if($articolo->pictures->count() > 5)
                    <li data-target="#carouselExampleIndicators" data-slide-to="5"></li>
                    @endif
                    @if($articolo->pictures->count() > 6)
                    <li data-target="#carouselExampleIndicators" data-slide-to="6"></li>
                    @endif
                    @if($articolo->pictures->count() > 7)
                    <li data-target="#carouselExampleIndicators" data-slide-to="7"></li>
                    @endif
                    @if($articolo->pictures->count() > 8)
                    <li data-target="#carouselExampleIndicators" data-slide-to="8"></li>
                    @endif
                    @if($articolo->pictures->count() > 9)
                    <li data-target="#carouselExampleIndicators" data-slide-to="9"></li>
                    @endif
                </ol>

                <div class="carousel-inner">
                    @foreach($articolo->pictures as $picture)
                        @if($articolo->pictures->first()->file == $picture->file)
                        <div class="carousel-item active">
                        @else
                        <div class="carousel-item">
                        @endif
                            <div class="col-md-4 offset-md-4" style="position: relative;">
                                <img src="{{$picture->getUrl(656,840)}}" alt="" class="img-fluid" style="max-height: 200px;">
                                <span style="position: absolute; top: 40%; left: 41%;" onclick="removeimg({{$picture->id}}, {{$articolo->id}})" class="btn btn-danger rounded-pill">rimuovi</span>
                            </div>
                            <!-- <span style="position: absolute; top: 80px; right: 515px;" onclick="removeimg({{$picture->id}}, {{$articolo->id}})" class="btn btn-danger d-none d-md-block rounded-pill">rimuovi</span>
                            <span style="position: absolute; top: 75px; right: 110px;" onclick="removeimg({{$picture->id}}, {{$articolo->id}})" class="btn btn-danger rounded-pill d-md-none">rimuovi</span> -->
                            
                        </div>
                    @endforeach
                        </div>
                    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
        </div>

{{-- Visualizzazione/modifica dati articolo--}}

        <form action="/admin/articoli/modifica/{{$articolo->id}}" method="POST">
            @csrf
            <div class="row mt-2">
                <div class="col-12 text-center">
                    <input type="text" class="rounded-pill text-center my-2" name="name" value="{{$articolo->name}}"><br>
                    <input type="text" class="rounded-pill text-center my-2" name="price" value="{{$articolo->price}}">
                    <div class="row mx-2">
                        <div class="col-12 text-center">
                            <hr class="border-white">
                            <h1>Gestione Taglie</h1>
                            @foreach($sizes as $size)
                                @foreach($articlesizes as $articlesize)
                                    @if($articlesize->size_id == $size->id)
                                        <div class="row">
                                            <div class="col-3 offset-3 col-md-2 offset-md-4 p-0 text-left">
                                                <input type="checkbox" checked name="size[]" class="form-check-input d-none" value="{{$size->name}}" id="{{$size->name}}">
                                                <label for="{{$size->name}}" class="form-check-label">{{$size->name}}</label>
                                            </div>
                                            <div class="col-5 offset-1 text-left">
                                                <input type="number" class="rounded-pill text-center" style="max-width: 50px;" min="0" max="100" name="qta-{{$size->name}}" value="{{$articlesize->qta}}"><hr class="m-1" style="border-color: transparent;">
                                            </div>
                                        </div>
                                    @endif
                                @endforeach
                            @endforeach
                        </div>
                    </div>
                    <div class="col-12 text-center">
                        <h3>Aggiungi taglia</h3>
                        <select name="addsize" class="rounded-pill my-2 p-0 pl-2 btn border-secondary bluepink dropdown-toggle" style="height: 25px;" value="{{ old('addsize') }}" id="sizetobeadded">
                            <option><h2> </h2></option>
                            @foreach($sizes as $size)
                            <option value="{{$size->id}}">
                                <h2>{{$size->name}}</h2>
                            </option>
                            <br>
                            @endforeach
                        </select><br>
                        <input type="number" class="rounded-pill text-center" style="max-width: 50px;" min="1" max="100" name="qta-addsize"><hr class="m-1" style="border-color: transparent;" id="qta-addsize">
                        <a class="btn btn-success rounded-pill mt-2 mb-3" onclick="addsize({{$articolo->id}})">Aggiungi</a>
                        <hr class="border-white">
                    </div>
                </div>
            </div>



            <div class="row mt-2">
                <div class="col-12 text-center">
                    <label for="description" class="m-0">Descrizione:</label>
                </div>
                <div class="col-12 text-center">
                    <textarea required name="description" class="bluepink btn border-secondary" maxlength="250" style="width: 75%; border-radius: 25px;" value="{{$articolo->description}}">{{$articolo->description}}</textarea>
                    @error('description') <p class="bg-danger">{{ $message }}</p> @enderror
                </div>
            </div>
            <div class="row my-5">
                <div class="col-12 text-center">
                    <button type="submit" class="btn btn-primary rounded-pill">Aggiorna</button><br>
                    <button class="btn btn-danger rounded-pill mt-3" type="button" data-toggle="modal" data-target="#exampleModal">Cancella</button>
                </div>
            </div>
        </form>

        <div class="modal fade mt-5" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document" style="bottom: -350px !important;">
                <div class="modal-content bg-dark" style="border-radius: 25px;">
                    <div class="modal-body text-center">
                        <p class="h2">Ne sei proprio sicura?</p>
                    </div>
                    <div class="col-12 text-center pb-4">
                        <button type="button" class="btn btn-danger mx-3" data-dismiss="modal">No</button>
                        <button type="button" class="btn btn-success mx-3" data-dismiss="modal" onclick="deletearticle({{$articolo->id}})">Si{{$articolo->id}}</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
