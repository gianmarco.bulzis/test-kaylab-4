<div class="container text-secondary">
     

{{-- DEVO RENDERE LA PAGINA SINGOLA PERCHé DROPZONE VEDE IL PRIMO ELEMENTO CON ID "drophere" QUINDI PRENDE QUELLO DESKTOP E MOBILE NON FUNZIONA --}}

{{-- For Mobile Devices --}}
    <ul class="nav nav-pills mb-3" id="admin-tab" role="tablist">
        <li class="nav-item" role="presentation">
            <a class="nav-link active" id="insert-article-tab" data-toggle="pill" href="#insert-article" role="tab" aria-controls="insert-article" aria-selected="true">Inserimento articoli</a>
        </li>
        <li class="nav-item" role="presentation">
            <a class="nav-link" id="insert-collection-tab" data-toggle="pill" href="#insert-collection" role="tab" aria-controls="insert-collection" aria-selected="false">Collezione</a>
        </li>
    </ul>
    <div class="tab-content" id="pills-tabContent">
        <div class="tab-pane fade show active" id="insert-article" role="tabpanel" aria-labelledby="admin-tab">
            <div class="row text-white tab-content" id="insert-article">
                <div class="col-12 text-center">
                    <form action="/admin/articoli/insert" method="POST" class="col-12 mb-5 shadow text-center" style=" border-radius: 10px; background-color: #FAEEE8;">
                    @csrf
                        <div class="container-fluid p-0 m-0 mt-3">
                            <h3 class="my-auto text-kaylab-p py-5">Inserimento Articoli</h3>
                            <input type="hidden" name="uniqueSecret" value="{{$uniqueSecret}}">
                        </div>
            
                        <div class="row justify-content-around">
                            <div class="col-12 col-md-4 text-center">
                                <label class="m-0 text-kaylab-p" for="catselect">Seleziona la categoria dell'articolo:</label><br>
                                <select name="catselect" required class="rounded-pill my-1 p-0 pl-2 btn shadow bg-grey dropdown-toggle" style="height: 25px;" value="{{ old('catselect') }}">
                                    <option><h2> </h2></option>
                                    @foreach($categories as $category)
                                    <option value="{{$category->name}}" class="text-dark">
                                        <h2>{{$category->name}}</h2>
                                    </option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-12 col-md-4 text-center">
                                <label class="m-0 text-kaylab-p text-primary" for="macroselect">Seleziona la macrocategoria dell'articolo:</label><br>
                                <select name="macroselect" class="rounded-pill my-2 p-0 pl-2 btn shadow bg-grey dropdown-toggle w-50" style="height: 25px;" value="{{ old('macroselect') }}">
                                    <option><h2> </h2></option>
                                    @foreach($macrocats as $macrocat)
                                    <option value="{{$macrocat->name}}">
                                        <h2>{{$macrocat->name}}</h2>
                                    </option>
                                    <br>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-12 col-md-4 text-center">
                                <label class="m-0 text-kaylab-p text-primary" for="collezione">Seleziona la collezione:</label><br>
                                <select name="collezione" class="rounded-pill my-2 p-0 pl-2 btn shadow bg-grey dropdown-toggle w-50" style="height: 25px;" id="" value="{{ old('collezione') }}">
                                    <option><h2></h2></option>
                                    @foreach($collezioni as $collezione)
                                    <option value="{{$collezione->id}}">
                                        <h2>{{$collezione->name}}</h2>
                                    </option>
                                    <br>
                                    @endforeach
                                </select>
                            </div>
                        </div>
            
                        {{-- Sizes Field --}}
                        
                        <hr>
            
                        <div class="row">
                            <div class="col-12 text-center">
                                <label class="text-kaylab-p bg-" for="">Seleziona le taglie dell'articolo</label><br>
                                <div class="row ml-1">
                                    @foreach($sizes->split($sizes->count()/5) as $row)
                                    <div class="col-md-3">
                                        @foreach($row as $size)
                                        <div class="row">
                                            <div class="col-4 offset-2 p-0 text-left">
            
                                                <input type="checkbox" name="size[]" class="form-check-input" value="{{$size->name}}" id="{{$size->name}}">
                                                <label for="{{$size->name}}" class="text-capitalize form-check-label taglie-checklist text-kaylab-p">{{$size->name}}</label>
                                            </div>
                                            <div class="col-4 offset-1 text-left">
            
                                                <input type="number" class=" text-left rounded-pill custom-increment text-center border-none shadow bg-grey form-control"  min="1" max="100" name="qta-{{$size->name}}"><hr class="m-1" style="border-color: transparent;">
                                            </div>
                                        </div>
                                        @endforeach
                                    </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
            
                        {{-- Name Field --}}
                        <hr>
                        <div class="row mt-2 justify-content-around">
                            <div class="col-4 text-center d-flex align-items-start justify-content-center">
                                <label for="name" class="m-0 text-kaylab-p mr-3" style="font-size: 18px; font-weight: bold;">Nome:</label>
                                <input required name="name" class="m-0 btn bg-grey shadow rounded-pill" value="{{ old('name') }}" style="height: 25px;">
                                @error('name') <p class="bg-danger">{{ $message }}</p> @enderror
                            </div>
                                
                            <div class="col-4 text-center">
                                <div class="d-flex align-items-start justify-content-center">
                                    <label class="text-kaylab-p mr-3" for="price" class="m-0" style="font-size: 18px; font-weight: bold;">Prezzo €:</label>
                                    <input required name="price" class="m-0 btn shadow bg-grey rounded-pill" type="number" min="1" max="1000" value="{{ old('price') }}" style="height: 25px;">
                                    @error('price') <p class="bg-danger">{{ $message }}</p> @enderror
                                </div>
                            </div>
                        </div>
            
                        {{-- Price Field --}}
            
                        <div class="row mt-2">
                        </div>
            
                        {{-- Description Field --}}
            
                        <div class="row mt-2">
                            <div class="col-12 text-center">
                                <label class="text-kaylab-p" for="description" class="m-0" style="font-size: 20px; font-weight: bold;">Descrizione:</label>
                            </div>
                            <div class="col-12 text-center">
                                <textarea required name="description" class="bg-grey shadow btn rounded text-left" maxlength="250" style="width: 75%; border-radius: 25px;" value="{{ old('description') }}"></textarea>
                                @error('description') <p class="bg-danger">{{ $message }}</p> @enderror
                            </div>
                        </div>
            
                        {{-- DROPZONE --}}
            
                        <label class="mt-4 h3 text-kaylab-p" for="dropzone">Inserisci qui le foto dell'articolo</label><br>
                        
                        <p class="pb-2 text-primary">
                            <i>(Attenzione, per il momento, una volta inserite le foto non è possibile aggiungerne altre. In tal caso eliminare l'articolo e reinserirlo.)</i>
                        </p>
            
                        <div class="d-flex justify-content-center">
                            <div class="dropzone shadow" id="my-awesome-dropzone"></div>
                        </div>
            
                        <button type="submit" id="btn-add" class="btn rounded-pill my-4 shadow text-light bg-grey text-kaylab-p w-25" ><b>Aggiungi<i class="fas fa-plus mx-1 plus"></i></b></button>
                    </form>
                </div>
            </div>
        </div>
        <div class="tab-pane fade" id="insert-collection" role="tabpanel" aria-labelledby="admin-tab">
            <div id="hello">
                <div class="col-12 text-center">
                    <form action="/admin/collection/insert" method="POST" id="collection-form" enctype='multipart/form-data' class="col-12 mb-5 shadow text-center" style=" border-radius: 10px; background-color: #FAEEE8;">
                        @csrf
                        <div class="container-fluid p-0 m-0 mt-3">
                            <h3 class="my-auto text-kaylab-p py-5">Crea una nuova collezione</h3>
                            <input type="hidden" name="uniqueSecret" value="{{$uniqueSecret}}">
                            <div class="row">
                                <input type="file" name="collectionpicture" class="d-none" id="collection-file">
                                <div class="col-12 text-center mb-4">
                                    <label for="collectionname" class="">Nome Collezione*</label><br>
                                    <input type="text" required minlength="3" maxlength="250" class="m-0 btn bg-grey shadow rounded-pill col-4" name="collectionname">
                                </div>
                                <div class="col-12 text-center mb-5">
                                    <label for="collectiondescription">Descrizione*</label>
                                    <textarea name="collectiondescription" required minlength="3" maxlength="500" class="bg-grey shadow btn form-control rounded text-left" cols="30" rows="10"></textarea>
                                </div>
                                <div class="col-12 text-center">
                                    <a class="btn rounded-pill shadow text-light bg-grey text-kaylab-p w-25" onclick="$('#collection-file').trigger('click')" id="collection-upload">Carica un'immagine</a><br>
                                    <span class="picture-info d-none">Immagine caricata</span><br>
                                    <input type="submit" class="btn rounded-pill my-4 shadow text-light bg-grey text-kaylab-p w-25" onclick="collectionFormSubmit()">
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    
    </div>
    {{--  --}}

    @if (session()->has('message'))
        <div class="alert alert-success text-center rounded-pill" id="articleadded">
            {{ session('message') }}
        </div>
    @endif

</div>
