@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row justify-content-center">
        @if(Auth::user())
            @if(Auth::user()->email_verified_at)
                <div class="col-12">verificata</div>
            @endif
        @endif
    </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div class="col-12 my-5 pt-5">
            @guest
            @else
                @if( Auth::user()->email == "admin@admin.it" )
                    @include('admin.insertarticle')
                @endif
            @endguest
        </div>
    </div>
    @include('articoli')
</div>
@endsection
