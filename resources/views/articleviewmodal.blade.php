<div class="container">
    <div class="row px-lg-5 py-4">
        <div class="col-12 col-lg-5">
            <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
                
                <div class="carousel-inner">
                    @foreach($article->pictures as $picture)
                        @if($picture == $article->pictures->first())
                        <div class="carousel-item active">
                            <img class="d-block w-100" src="{{$picture->getUrl(656,840)}}" alt="First slide">
                        </div>
                        @else
                        <div class="carousel-item">
                            <img class="d-block w-100" src="{{$picture->getUrl(656,840)}}" alt="Second slide">
                        </div>
                        @endif
                    @endforeach
                </div>
                <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                  <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                  <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                  <span class="carousel-control-next-icon" aria-hidden="true"></span>
                  <span class="sr-only">Next</span>
                </a>
            </div>

            <div class="col-12 mt-3 text-center" id="articleslider">
                <?php $i = 0 ?>
                @foreach($article->pictures as $picture)
                    <div class="d-flex justify-content-center">
                        <img src="{{$picture->getUrl(50,85)}}" class="img-fluid" data-target="#carouselExampleControls" data-slide-to="<?php echo($i); ?>" alt="">
                        <?php $i += 1; ?>
                    </div>
                @endforeach
            </div>
            {{-- <img src="{{$article->pictures->first()->getUrl(656,840)}}" class="img-fluid" alt=""> --}}
        </div>
        <div class="col-12 col-lg-7">
            <h3 class="font-weight-semi-bold d-none d-lg-block">{{$article->name}}</h3>
            <h5 class="font-weight-semi-bold d-lg-none">{{$article->name}}</h5>
            <div class="row px-3 my-3">
                <select name="sizeselect" id="sizeselect" class="rounded-pill mb-5 mr-2 p-0 pl-2 btn border-secondary dropdown-toggle" style="height: 25px;" value="{{ old('sizeselect') }}">
                    <option><h2>Taglia</h2></option>
                    @foreach($sizes as $size)
                        <option>{{$size}}</option>
                    @endforeach
                </select>
                <select class="mb-5 rounded-pill bg-white mx-2" id="qtainput">
                    <option>1</option>
                    <option>2</option>
                    <option>3</option>
                    <option>4</option>
                    <option>5</option>
                    <option>6</option>
                    <option>7</option>
                    <option>8</option>
                    <option>9</option>
                    <option>10</option>
                </select>
            </div>
            <div class="row px-3 my-3">
                <p class="font-weight-regular">{{$article->description}}</p>
            </div>
            @guest
            <a href="{{route('loginregister')}}" class="btn text-kaylab-e btn-outline-kaylab-e rounded-pill">Aggiungi al carrello</a>
            @else
            <a class="btn text-kaylab-e btn-outline-kaylab-e rounded-pill" data-dismiss="modal" onclick="addtocart({{$article->id}}, {{$allsizes}}, {{Auth::user()}})">Aggiungi al carrello</a>
            @endguest
        </div>
    </div>
</div>

