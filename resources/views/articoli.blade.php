<div class="container-fluid m-0 p-0">
    <div class="row mt-2 mb-4">
        <div class="col-12 text-center">
            <button class="btn btn-danger rounded-pill mx-1" onclick="adminfiltrotutti('prova')">Tutti</button><br>
            <select name="macroselect" required class="rounded-pill my-2 p-0 pl-2 btn border-secondary bluepink dropdown-toggle" style="height: 25px;" value="{{ old('macroselect') }}" id="macroselectfilter">
                <option><h2>Tutte</h2></option>
                @foreach($macrocats as $macrocat)
                <option value="{{$macrocat->name}}">
                    <h2>{{$macrocat->name}}</h2>
                </option>
                <br>
                @endforeach
            </select><br>
            <select name="catselect" required class="rounded-pill my-1 p-0 pl-2 btn border-secondary bluepink dropdown-toggle" style="height: 25px;" value="{{ old('catselect') }}" id="catselectfilter">
                <option><h2>Tutte</h2></option>
                @foreach($categories as $category)
                <option value="{{$category->name}}" class="text-dark">
                    <h2>{{$category->name}}</h2>
                </option>
                @endforeach
            </select>
        </div>

        <div class="col-12 text-center">
            <button class="btn btn-warning rounded-pill" onclick="adminfilter()">Filtra</button>
        </div>
    </div>
    <div class="row my-2 mx-0 mx-md-5" id="articles-container">
        {{-- @foreach($articoli as $articolo)
        <div class="col-6 col-md-3 mx-0 px-1">
            <div class="card m-0 mb-4 p-0 border-0 bg-transparent text-left">
                @if(count($articolo->pictures)>0)
                <a href="javascript:showarticle({{$articolo->id}})">
                    <img class="card-img-top img-fluid mx-auto" src="{{$articolo->pictures->first()->getUrl(656,840)}}" alt="">
                </a>
                @else
                <img class="card-img-top img-fluid mx-auto" src="http://placehold.it/150" alt="">
                @endif
                <div class="card-body mt-3 p-0">
                  <h5 class="card-title m-0" style="white-space: nowrap; overflow: hidden;">{{Str::limit($articolo->name, 15, ' [...]')}}</h5>
                  <h5 class="font-weight-bold mt-2">€{{$articolo->price}}.00</h5>
                </div>
            </div>
        </div>
        @endforeach --}}
        @include('articolifiltrati')
    </div>
</div>
