@extends('layouts.app')
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				@if ($message = Session::get('success'))
				<div class="alert alert-success alert-block">
					<button type="button" class="close" data-dismiss="alert">×</button>
						<strong>{{ $message }}</strong>
				</div>
				@endif
				@if ($message = Session::get('error'))
				<div class="alert alert-danger alert-block">
					<button type="button" class="close" data-dismiss="alert">×</button>
						<strong>{{ $message }}</strong>
				</div>
				@endif
			</div>
		</div>	
  
        <form method="post" action="{{url('newsletter')}}">
        @csrf
		<div class="row">
			<div class="col-md-4"></div>
			<div class="form-group col-md-2">
			  <label for="Email">Email:</label>
			  <input type="text" class="form-control" name="email">
			</div>
		</div>		  
		<div class="row">
			<div class="col-md-4"></div>
			<div class="form-group col-md-4">
				<button type="submit" class="btn btn-success">Submit</button>
			</div>
		</div>
      </form>
	</div>