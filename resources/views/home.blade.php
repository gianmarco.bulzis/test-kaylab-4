@extends('layouts.app')

@section('content')

{{-- NEWSLETTER MESSAGE ERROR --}}
@if ($message = Session::get('success'))
        <div id="alertCust" class="alert alert-success alert-block">
            <button type="button" class="close" data-dismiss="alert">&nbsp×</button>
                <strong>{{ $message }}</strong>
        </div>
        @endif
        @if ($message = Session::get('error'))
        <div id="alertCust" class="alertCust alert alert-danger alert-block">
            <button type="button" class="close" data-dismiss="alert">&nbsp×</button>
                <strong>{{ $message }}</strong>
        </div>
        @endif


<div class="container-fluid pt-lg-4 mt-lg-5 px-0" id="hero-container">
    <div class="row no-gutters justify-content-center">

        <div class="col-12">
            <div id="slider-slick" class="multiple-items">
                @foreach ($collezioni as $collezione)
                <div class="">
                    <div class="row no-gutters radius-lg-30 overflow-hidden h-v60">
                        <div class="col-12 col-lg-8" style="background: url({{$collezione->picture}}); background-size: cover; background-position: top center;">
                            <div class="d-flex d-lg-none h-100 justify-content-center align-items-center">
                                <div class="">
                                    <a href="{{route('shop', ['collection' => $collezione->id])}}" class="btn btn-light rounded-pill px-5 py-3 shadow-sm">SCOPRI ORA</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-lg-4 cover-side d-none d-lg-block">
                            <div class="pt-5 mt-5 mx-5">
                                <h6 class="text-white font-weight-regular">NUOVA COLLEZIONE</h6>
                                <h2 class="font-weight-semi-bold text-white mt-3 mb-5 ml-2">{{$collezione->name}}</h2>
                                <h6 class="font-weight-medium text-white">{{$collezione->description}}</h6>
                            </div>
                            <div class="d-flex flex-column arrow-div">
                                <a class="slickNext my-2">
                                    <img src="/images/icons/arrow-next.png" alt="">
                                </a>
                                <a class="slickPrev mt-2">
                                    <img src="/images/icons/arrow-prev.png" alt="">
                                </a>
                            </div>
                            <a href="{{route('shop', ['collection' => $collezione->id])}}" class="btn btn-light rounded-pill slick-slide-button px-5 py-3 ml-5 shadow-sm">SCOPRI ORA</a>
                        </div>
                    </div>
                </div>
                @endforeach
                {{-- <div>
                    <div class="row no-gutters radius-30 overflow-hidden h-v60">
                        <div class="col-12 col-lg-8" style="background: url('/images/hero/Copertina2.jpeg'); background-size: cover; background-position: top center;">
                        </div>
                        <div class="col-12 col-lg-4 cover-side">
                            <div class="pt-5 mt-5 mx-5">
                                <h6 class="text-white font-weight-regular">NUOVA COLLEZIONE</h6>
                                <h2 class="font-weight-semi-bold text-white mt-3 mb-5 ml-2">Safari Camp</h2>
                                <h6 class="font-weight-medium text-white">Vacay-Ready Outfits for Summer Adventures</h6>
                            </div>
                            <div class="d-flex flex-column arrow-div">
                                <a class="slickNext my-2">
                                    <img src="/images/icons/arrow-next.png" alt="">
                                </a>
                                <a class="slickPrev mt-2">
                                    <img src="/images/icons/arrow-prev.png" alt="">
                                </a>
                            </div>
                            <a href="" class="btn btn-light rounded-pill slick-slide-button px-5 py-3 ml-5">SCOPRI ORA</a>
                        </div>
                    </div>
                </div>
                <div>
                    <div class="row no-gutters radius-30 overflow-hidden h-v60">
                        <div class="col-12 col-lg-8" style="background: url('/images/hero/pexels-photo-1094072.jpeg'); background-size: cover; background-position: top center;">
                        </div>
                        <div class="col-12 col-lg-4 cover-side">
                            <div class="pt-5 mt-5 mx-5">
                                <h6 class="text-white font-weight-regular">NUOVA COLLEZIONE</h6>
                                <h2 class="font-weight-semi-bold text-white mt-3 mb-5 ml-2">Safari Camp</h2>
                                <h6 class="font-weight-medium text-white">Vacay-Ready Outfits for Summer Adventures</h6>
                            </div>
                            <div class="d-flex flex-column arrow-div">
                                <a class="slickNext my-2">
                                    <img src="/images/icons/arrow-next.png" alt="">
                                </a>
                                <a class="slickPrev mt-2">
                                    <img src="/images/icons/arrow-prev.png" alt="">
                                </a>
                            </div>
                            <a href="" class="btn btn-light rounded-pill slick-slide-button px-5 py-3 ml-5">SCOPRI ORA</a>
                        </div>
                    </div>
                </div>
                <div>
                    <div class="row no-gutters radius-30 overflow-hidden h-v60">
                        <div class="col-12 col-lg-8" style="background: url('/images/hero/pexels-photo-4127362.jpeg'); background-size: cover; background-position: top center;">
                        </div>
                        <div class="col-12 col-lg-4 cover-side">
                            <div class="pt-5 mt-5 mx-5">
                                <h6 class="text-white font-weight-regular">NUOVA COLLEZIONE</h6>
                                <h2 class="font-weight-semi-bold text-white mt-3 mb-5 ml-2">Safari Camp</h2>
                                <h6 class="font-weight-medium text-white">Vacay-Ready Outfits for Summer Adventures</h6>
                            </div>
                            <div class="d-flex flex-column arrow-div">
                                <a class="slickNext my-2">
                                    <img src="/images/icons/arrow-next.png" alt="">
                                </a>
                                <a class="slickPrev mt-2">
                                    <img src="/images/icons/arrow-prev.png" alt="">
                                </a>
                            </div>
                            <a href="" class="btn btn-light rounded-pill slick-slide-button px-5 py-3 ml-5">SCOPRI ORA</a>
                        </div>
                    </div>
                </div>
                <div>
                    <div class="row no-gutters radius-30 overflow-hidden h-v60">
                        <div class="col-12 col-lg-8" style="background: url('/images/hero/little-fashionable-rebel-girl-dressed-in-dress.jpg'); background-size: cover; background-position: top center;">
                        </div>
                        <div class="col-12 col-lg-4 cover-side">
                            <div class="pt-5 mt-5 mx-5">
                                <h6 class="text-white font-weight-regular">NUOVA COLLEZIONE</h6>
                                <h2 class="font-weight-semi-bold text-white mt-3 mb-5 ml-2">Safari Camp</h2>
                                <h6 class="font-weight-medium text-white">Vacay-Ready Outfits for Summer Adventures</h6>
                            </div>
                            <div class="d-flex flex-column arrow-div">
                                <a class="slickNext my-2">
                                    <img src="/images/icons/arrow-next.png" alt="">
                                </a>
                                <a class="slickPrev mt-2">
                                    <img src="/images/icons/arrow-prev.png" alt="">
                                </a>
                            </div>
                            <a href="" class="btn btn-light rounded-pill slick-slide-button px-5 py-3 ml-5">SCOPRI ORA</a>
                        </div>
                    </div>
                </div> --}}
            </div>
        </div>    
    </div>
</div>

<div class="container my-5 pt-5">
    <div class="row">
        <div class="col-12 col-lg-8 offset-lg-2 text-center">
            <div class="radius-15 mx-5" id="discover-div">
                <h4 class="text-kaylab-a font-weight-bold mb-4"><span class="titolo">Scopri</span> <span class="titolo-vibur">Kaylab</span></h4>
                <p class="mb-4 font-14">Crafted from a love of motherhood, vintage textiles and sunny days, Kaylab makes<br>vintage-meets-boho children’s clothes that are designed to last.</p>
                <div class="d-flex justify-content-center">
                    <a href="" class="kaylab-link">SCOPRI CHI SIAMO</a>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container mt-8" id="macrocat-container">
    <div class="row">
        <div class="col-12 col-lg-6">
            <div class="row">
                <div class="col-12 col-sm-6 px-2">
                    <a href="{{route('shop', ['group' => 'Neonato'])}}">
                        <div class="position-relative">
                            <img src="/images/home/neonato-100.jpg" class="img-fluid radius-15 w-100 home-category-cover h-100" alt="">
                            <div class="macrocat-label position-absolute d-flex align-items-end radius-15 pl-3 pb-2">
                                <p class="text-white">Neonato</p>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-12 col-sm-6 px-2">
                    <a href="{{route('shop', ['group' => 'Neonata'])}}">
                        <div class="position-relative">
                            <img src="/images/home/neonata-100.jpg" class="img-fluid radius-15 w-100 home-category-cover" alt="">
                            <div class="macrocat-label position-absolute d-flex align-items-end radius-15 pl-3 pb-2">
                                <p class="text-white">Neonata</p>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-12 mt-3">
                    <a href="{{route('shop', ['group' => 'Bimba'])}}">
                        <div class="position-relative">
                            <img src="/images/home/bimba-100.jpg" class="img-fluid radius-15 w-100 home-category-cover" alt="">
                            <div class="macrocat-label position-absolute d-flex align-items-end radius-15 pl-3 pb-2">
                                <p class="text-white">Bimba</p>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-12 col-lg-6">
            <div class="row">
                <div class="col-12 mb-3">
                    <a href="{{route('shop', ['group' => 'Bimbo'])}}">
                        <div class="position-relative">
                            <img src="/images/home/bimbo-100.jpg" class="img-fluid radius-15 w-100 home-category-cover" alt="">
                            <div class="macrocat-label position-absolute d-flex align-items-end radius-15 pl-3 pb-2">
                                <p class="text-white">Bimbo</p>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-12 col-sm-6 px-2">
                    <a href="{{route('shop', ['group' => 'Minime'])}}">
                        <div class="position-relative">
                            <img src="/images/home/minime-100.jpg" class="img-fluid radius-15 w-100 home-category-cover" alt="">
                            <div class="macrocat-label position-absolute d-flex align-items-end radius-15 pl-3 pb-2">
                                <p class="text-white">Minime</p>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-12 col-sm-6 px-2">
                    <a href="{{route('shop')}}">
                        <div class="position-relative">
                            <img src="/images/home/newcollection-100.jpg" class="img-fluid radius-15 w-100 home-category-cover" alt="">
                            <div class="macrocat-label position-absolute d-flex align-items-end radius-15 pl-3 pb-2">
                                <p class="text-white">Nuova Collezione</p>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="row mt-xl-8">
        <div class="col-12 text-center">
            <h4 class="text-kaylab-a font-weight-bold mb-4"><span class="titolo">Nuovi</span> <span class="titolo-vibur">Arrivi</span></h4>
        </div>
        <div class="col-12" id="nuovi-articles-carousel">
            @foreach ($nuovi as $nuovo)
                <div class="px-2">
                    <a href="{{route('shoparticle', ['id' => base64_encode($nuovo->id . '_' . $nuovo->name)])}}" ondblclick="return false;">
                        <div class="shadow article-card bg-white">
                            <div class="article-img m-3 m-xl-4">
                                <img src="{{$nuovo->pictures->first()->getUrl(340,340)}}" alt="{{$nuovo->name}}">
                            </div>
                            <div class="article-info pl-3 pl-xl-4 pr-3 pr-xl-4 pb-3 pb-xl-4">
                                <h4 class="article-title small font-weight-medium text-primary text-truncate">{{$nuovo->name}}</h4>
                                <h6 class="article-price text-primary">{{$nuovo->price}},00€</h6>
                                <p><a>Visualizza <i class="fas fa-arrow-right fa-fw"></i></a></p>
                            </div>
                        </div>
                    </a>
                </div>
            @endforeach
        </div>
    </div>
</div>

<div class="container">
    <div class="row mt-4 px-2 justify-content-center">
        @foreach ($articoli as $articolo)

            <div class="card m-0 p-0 border-0 bg-transparent text-left">
                @if(count($articolo->pictures)>0)
                <a href="{{route('shoparticle', [$articolo->id])}}"style="cursor: pointer;">
                    <img class="card-img-top radius-15 img-fluid mx-auto shadow-lg" src="{{$articolo->pictures->first()->getUrl(200,256)}}" alt="{{$articolo->name}}">
                </a>
                @else
                <img class="card-img-top img-fluid mx-auto" src="http://placehold.it/150" alt="">
                @endif
                <div class="card-body mt-3 p-0">
                    <h5 class="card-title m-0" style="white-space: nowrap; overflow: hidden;">{{Str::limit($articolo->name, 15, ' [...]')}}</h5>
                    <h5 class="mt-2">€{{$articolo->price}}.00</h5>
                </div>
            </div>

        @endforeach
    </div>

    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="padding-right: 0 !important;">
        <div class="modal-dialog" role="document" style="min-width: 75vw !important;">
          <div class="modal-content radius-15 mt-5" style="position: relative;">
            <div id="articleModalCloser">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="py-3" id="articleViewContainer">
              
            </div>
          </div>
        </div>
    </div>
</div>
{{-- NEWSLETTER --}}

<div class="container-fluid bg-kaylab-primary py-5">
        <form method="POST" action="{{url('newsletter')}}">
            @csrf
    <div class="row justify-content-center text-center font-25 font-weight-semi-bold text-white">
        <div class="col-12 col-md-8 col-xl-4">
            <span>Iscriviti alla nostra Newsletter<br>per non perderti le migliori offerte</span>
                <div class="d-flex justify-content-center align-items-center mt-4">
                    <input id="newsMail" type="text" name="email" class="form-control border2 rounded-pill w-50 mr-3 bg-white p-30">
                    <button type="submit" class="btn px-3 py-2 btn-primary">Registrati</button>
                </div>
        </div>
    </div>
</form>
</div>

@endsection
@push('scripts')
<script>
    $(document).ready(function(){
        $('#slider-slick').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false,
            fade: false,
            dots: false,
            autoplay: false,
            autoplaySpeed: 4000
        });
        var indexVal=1;
        $(".slickNext").click(function(e){
            e.preventDefault();
            indexVal=$('#slider-slick').slick('slickCurrentSlide');
            $('#slider-slick').slick('slickGoTo', indexVal+1);
            indexVal=indexVal+1;
            if(indexVal>5){
                indexVal=1;
            }
        });
        $(".slickPrev").click(function(e){
            e.preventDefault();
            indexVal=$('#slider-slick').slick('slickCurrentSlide');
            $('#slider-slick').slick('slickGoTo', indexVal-1);
            indexVal=indexVal+1;
            if(indexVal<1){
                indexVal=1;
            }
        });

        
    });
</script>
@endpush
