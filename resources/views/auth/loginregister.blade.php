@extends('layouts.app')

@section('content')
<div class="container mt-5">
    <div class="row">
        <div class="col-12 col-lg-10 offset-lg-1">
            <div class="bg-white shadow pb-5 pt-4" id="form-container">
                <div class="row px-5">

<!-- Login -->

                    <div class="col-12 col-lg-6 text-left pl-lg-4 pr-5">
                        <h4 class="text-kaylab-a mt-2 mb-2 mb-lg-5 font-weight-bold">Login</h4>
                        <p class="text-kaylab-a mb-0">Bentornato! Inserisci i tuoi dati</p>
                        <form class="pr-4 py-4" method="POST" action="{{ route('login') }}">
                            @csrf
                            <p class="text-kaylab-a mb-1">Email</p>
        
                            <input id="email" type="email" class="mb-3 py-4 form-control form-control-2 @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
        
                            @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
        
                            <p class="text-kaylab-a mb-1">Password</p>
        
                            <input id="password" type="password" class="mb-3 py-4 form-control form-control-2 @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">
        
                            @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
        
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
        
                                <p class="text-kaylab-b mb-0">
                                    Ricordami
                                </p>
                            </div>
        
                            <button type="submit" class="btn bg-kaylab-g text-white mt-2 mt-lg-3 px-5" style="font-size: 16px;">
                                {{ __('Login') }}
                            </button>
        
                            @if (Route::has('password.request'))
                                <a class="btn btn-link text-kaylab-a mt-0 mt-lg-3 px-0" href="{{ route('password.request') }}">
                                    Hai dimenticato la tua password?
                                </a>
                            @endif
                        </form>
                    </div>
        
<!-- Registrazione -->
        
                    <div class="col-12 col-lg-6 text-left pr-4 pl-lg-5">
                        <h4 class="text-kaylab-a mt-2 mb-2 mb-lg-5 font-weight-bold">Registrati</h4>
                        <p class="text-kaylab-a mb-0">Iscriviti a Kaylab!</p>                   
                        <form class="pr-3 py-lg-4 pt-2" method="POST" action="{{ route('register') }}">
                            @csrf
        
                            {{-- <p class="text-kaylab-b mb-1">Nome e cognome</p>
        
                            <input id="name" type="text" class="form-control mb-3 py-4 @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>
        
                            @error('name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror --}}
        
                            <p class="text-kaylab-b mb-1">Email*</p>
        
                            <input id="email" type="email" class="form-control form-control-2 mb-3 py-4 @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">
        
                            @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
        
                            <p class="text-kaylab-b mb-1">Password*</p>
        
                            <input id="password" type="password" class="form-control form-control-2 mb-3 py-4 @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">
        
                            @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
        
                            <p class="text-kaylab-b mb-1">Ripeti la password*</p>
        
                            <input id="password-confirm" type="password" class="form-control form-control-2 mb-3 py-4" name="password_confirmation" required autocomplete="new-password">
        
                            <button type="submit" class="btn bg-kaylab-g text-white mt-3 px-5" style="font-size: 16px;">
                                Iscriviti
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </div>        
    </div>

        <!-- Login -->        
</div>
@endsection
