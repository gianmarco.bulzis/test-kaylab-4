@foreach($articoli as $articolo)
<div class="col-6 col-md-2">
    <div class="card m-0 p-0 border-0 bg-transparent text-left">
        @if(count($articolo->pictures)>0)
        <a href="javascript:showarticle({{$articolo->id}})">
            <img class="card-img-top img-fluid mx-auto shadow-lg" src="{{$articolo->pictures->first()->getUrl(656,840)}}" alt="">
        </a>
        @else
        <img class="card-img-top img-fluid mx-auto" src="http://placehold.it/150" alt="">
        @endif
        <div class="card-body mt-3 p-0">
            <h5 class="card-title m-0" style="white-space: nowrap; overflow: hidden;">{{Str::limit($articolo->name, 15, ' [...]')}}</h5>
            <h5 class="mt-2">€{{$articolo->price}}.00</h5>
        </div>
    </div>
    <div class="col-12 text-center mb-2">
        <a href="javascript:showarticle({{$articolo->id}})" class="btn btn-primary rounded-pill">modifica</a>

        {{-- MODAL CONFERMA CANCELLAZIONE ARTICOLO --}}

    </div>
</div>
@endforeach
