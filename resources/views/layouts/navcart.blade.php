<?php $total = 0; ?>
@if(count(Auth::user()->carts) > 0)
    @foreach(Auth::user()->carts as $cart)
        <?php $total += $cart->total() ?>
        <div class="d-flex m-0 p-2">
            <img class="img-fluid radius-10 mr-3" src="{{$cart->article->pictures->first()->getUrl(90,120)}}" alt="">
            <div class="my-auto w-100">
                <p class="font-weight-medium text-kaylab-a mb-1 text-left text-truncate">{{$cart->article->name}}</p>
                <p class="font-weight-semi-bold text-kaylab-a mb-1 text-left">{{$cart->article->price}},00€</p>
                <p class="font-weight-medium text-kaylab-a mb-1 text-left">Taglia: {{$cart->size->name}}</p>
                <div class="d-flex mb-1 align-items-center justify-content-between">
                    <div class="d-flex align-items-center">
                        <a class="mr-2" onclick="changeqta('dif', {{$cart->id}}, {{count(Auth::user()->carts)}})"><i class="fas text-kaylab-e fa-minus-circle"></i></a>
                        <p class="font-weight-medium text-kaylab-a mb-0 text-left" id="cart<?php echo $cart->id ?>qta">{{$cart->qta}}</p>
                        <a class="ml-2" onclick="changeqta('sum', {{$cart->id}}, )"><i name="plus" class="fas text-kaylab-e fa-plus-circle"></i></a>
                        <?php if(isset($messaggio) && $cart->id == $cid){ ?>
                            <span class="text-danger ml-3 small" id="messaggio">{{ $messaggio }}</span>
                        <?php } ?>
                    </div>
                    <a onclick="removeFromCart({{$cart->id}}, {{$cart->qta}})"><i class="far text-kaylab-b fa-trash-alt"></i></a>
                </div>
                <p class="font-weight-medium text-kaylab-a mb-0 ml-5 text-right">Totale: <span class="font-weight-bold">{{$cart->total()}},00€</span></p>
            </div>
        </div>
    @endforeach
        @if($total < 65 && count(Auth::user()->carts) > 0)
            <div class="text-center pt-2">
                <p class="m-0 text-kaylab-f font-weight-semi-bold">Aggiungi <span class="font-weight-bold"><?php echo(65 - $total) ?>,00€</span> al tuo ordine per usufruire della spedizione gratuita</p>
            </div>
        @else
        <div class="text-center pt-2">
            <p class="m-0 text-kaylab-g font-weight-semi-bold">Idoneo alla spedizione gratuita</p>
        </div>
        @endif
        <div class="w-100 bg-kaylab-light text-white w-100 px-2 py-3 d-flex justify-content-between align-items-center">
            <h6 class="m-0 mt-2 text-primary font-weight-semi-bold">Totale: <span class="text-kaylab-f font-weight-semi-bold">{{$total}},00€</span></h6>
            <a href="{{route('riepilogo')}}" class="btn bg-white text-primary radius-15 font-weight-semi-bold">Ordina</a>
        </div>
@else
    <div class="text-center py-3">
        <i class="fas fa-shopping-bag fa-3x text-dark"></i>
        <h5 class="text-dark mt-3">Il tuo carrello è vuoto</h5>
    </div>
@endif