<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Jost:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Vibur&display=swap" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <link rel="stylesheet" href="/dropzone-5.7.0/dist/dropzone.css">
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>
    <script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>

</head>
<body>
    <div id="app">
        @include('layouts.navbar')

        <main class="" id="main">
            @yield('content')
        </main>

        @include('layouts.footer-navbar')
        @include('layouts.footer')

    </div>
    
    <script src="https://kit.fontawesome.com/adaaf60c9d.js" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    
    <script src="/js/script.js"></script>
    <script src="/js/profile.js"></script>
    <script src="/js/user.js"></script>
    <script src="/js/admin.js"></script>
    <script src="/js/shop.js"></script>

    <script src="/dropzone-5.7.0/dist/dropzone.js"></script>
    <script>

        Dropzone.autoDiscover = false;

        $(document).ready(function(){

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            if($(window).width() < 765){
                $('#footer-navbar').removeClass('d-none');
            }

            if($('#my-awesome-dropzone').length > 0){
            let csrfToken = $('meta[name="csrf-token"]').attr('content');
            let uniqueSecret = $('input[name="uniqueSecret"]').attr('value');

            let myDropzone = new Dropzone('#my-awesome-dropzone', {
                url: '/admin/articoli/immagini/upload',

                params: {
                    _token: csrfToken,
                    uniqueSecret: uniqueSecret
                },

                addRemoveLinks: true,

                _init: function () {
                    $.ajax({
                        type: 'GET',
                        url: '/admin/articoli/immagini/reload',
                        data: {
                            uniqueSecret: uniqueSecret
                        },
                        dataType: 'json'
                    }).done(function (data) {
                        $.each(data, function (key, value) {
                            let file = {
                                serverId: value.id
                            };
                            myDropzone.options.addedfile.call(myDropzone, file);
                            myDropzone.options.thumbnail.call(myDropzone, file, value.src);
                        });
                    });
                },
                get init() {
                    return this._init;
                },
                set init(value) {
                    this._init = value;
                },

            });

            myDropzone.on("success", function(file, response){
                file.serverId = response.id;
                dzpreviews = document.getElementsByClassName('dz-preview');
                dzremoves = document.getElementsByClassName('dz-remove');
            });

            myDropzone.on("removedfile", function(file){
                $.ajax({
                    type: 'DELETE',
                    url: '/admin/articoli/immagini/remove',
                    data: {
                        _token: csrfToken,
                        id: file.serverId,
                        uniqueSecret: uniqueSecret
                    },
                    dataType: 'json'
                });
            });
            }
        });

    </script>

    <script>
        $(function(){
            $(document).click(function(e){
                if(!$('#risultato-ricerca').has(e.target).length > 0 && e.target.id != 'risultato-ricerca' && !$('#risultato-ricerca').hasClass('d-none')){
                    $('#risultato-ricerca').addClass('d-none');
                    $('#search-input').val('');
                }
            });
        });
    </script>

    <script>

        function articleslide(){
            setTimeout(function(){
                $('#articleslider').slick({
                    slidesToShow: 4,
                    slidesToScroll: 1,
                    arrows: false,
                    fade: false,
                    dots: false
                });
            }, 1000);
        }


    </script>
    @stack('scripts')
</body>
</html>
