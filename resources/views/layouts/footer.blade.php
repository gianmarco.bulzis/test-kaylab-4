

<div class="container-fluid py-5 bg-kaylab-secondary" id="footer">
    <div class="row no-gutters w-100 px-2 px-lg-5">
        <div class="col-12 text-center">
            <div class="d-flex justify-content-center" id="footerLinksDiv">
                <div class="text-left mx-4 mx-lg-5 mb-4 mb-lg-5">
                    <h6 class="mb-2 mb-lg-4 text-white f-header">Kaylab Srl</h6>
                    <a href=""><p class="mb-0 text-white f-text">Chi Siamo</p></a>
                    <a href=""><p class="mb-0 text-white f-text">Domande Frequenti</p></a>
                    <a href=""><p class="mb-0 text-white f-text">Cookie & Privacy Policy</p></a>
                    <a href=""><p class="mb-0 text-white f-text">Termini e Condizioni</p></a>
                    <a href=""><p class="mb-0 text-white f-text">Contatti</p></a>
                </div>
                <div class="text-left mx-5 mb-5">
                    <h6 class="mb-2 mb-lg-4 text-white f-header">Menù Rapido</h6>
                    <a href=""><p class="mb-0 text-white f-text">Neonato</p></a>
                    <a href=""><p class="mb-0 text-white f-text">Neonata</p></a>
                    <a href=""><p class="mb-0 text-white f-text">Bimbo</p></a>
                    <a href=""><p class="mb-0 text-white f-text">Bimba</p></a>
                    <a href=""><p class="mb-0 text-white f-text">Mini Me</p></a>
                </div>
            </div>
        </div>
        <div class="col-12 text-center">
            <h5 class="text-white">Seguici sui Social</h5>
            <div class="d-flex justify-content-center my-3">
                <a href="" class="mx-2 mx-lg-3"><i class="fab text-white fa-facebook social-icon"></i></a>
                <a href="" class="mx-2 mx-lg-3"><i class="fab text-white fa-instagram social-icon"></i></a>
            </div>
        </div>
        <div class="col-12 text-right mt-5 px-2 px-lg-5 bg-kaylab-secondary">
            <h6 class="text-white"><i>© 2021 Kaylab</i></h6>
        </div>
    </div>
</div>