
<!-- Navbar superiore -->
<div class="container-fluid py-3 d-none d-lg-block bg-kaylab-primary" id="upper-navbar">
    <div class="row px-5 justify-content-between">
        <div class="d-flex align-items-center">
            <a href="{{route('home')}}" class="px-3 upper-link text-px-3 text-uppercase dark-text">kaylab.com</a>
            <p class="m-0 upper-link px-3 text-uppercase dark-text">consegna gratuita per ordini da 65€</p>
        </div>
        <div class="d-flex align-items-center">
            <a href="tel:+39331217840" class="px-3 upper-link text-uppercase dark-text">+39331217840</a>
            <a href="facebook" class="px-1"><i class="fab fa-instagram dark-text" style="font-size: 18px;"></i></a>
            <a href="facebook" class="px-1"><i class="fab fa-facebook-square dark-text" style="font-size: 18px;"></i></a>
        </div>
    </div>
</div>

<!-- Fixed Navbar -->
<nav class="navbar navbar-expand-lg navbar-light bg-light" id="fixednavbar">
    <div class="d-flex navbar-mobile fixed-top d-lg-none w-100 px-4 px-sm-5 justify-content-between align-items center bg-white shadow">
        <a href="{{route('home')}}" class="d-flex align-items-center">
            <img src="/images/Logo.png" class="h-50" alt="">
        </a>
        <button class="navbar-toggler" onclick="opensidebar()">
            <span class="navbar-toggler-icon"></span>
        </button>
    </div>

    <div class="sidebar bg-kaylab-g pt-4" id="sidebar">
        <a class="text-light" onclick="closesidebar()" id="sidebarCloser"><i class="fas fa-times fa-2x text-white"></i></a>
        <div class="d-flex flex-column text-center">
            <a href="/" class="mb-5 text-white w-50 mx-auto"><img src="/images/logomini.png" alt="" class="img-fluid" id="logokaylab"></a>
            @auth
                @if(Auth::user()->email == 'admin@admin.it')
                <a href="{{route('admin')}}" class="my-2 text-white"><h5>Pannello di controllo</h5></a>
                @endif
            @endauth
            <a href="<?php if(Auth::user()){echo('profile');}else{echo('/loginregister');} ?>" class="my-2 text-white"><h5>Profilo</h5></a>
            <a href="" class="my-2 text-white"><h5>Carrello
            @auth
                    @if(Auth::user()->cartelements() > 0)
                    <span id="itemsInCartMobile" class="font-weight-semi-bold ml-2 bg-kaylab-e text-white">{{Auth::user()->cartelements()}}</span>
                    @endif
                @endauth
            </h5></a>
            <a href="" class="my-2 text-white"><h5>I miei ordini</h5></a>
            <a href="" class="my-2 text-white"><h5>Preferiti</h5></a>
            <a href="" class="my-2 text-white"><h5>Assistenza</h5></a>
        </div>
        <div class="d-flex w-100 justify-content-center align-items-center" style="position: absolute; bottom: 30px;">
            <a href="" class="mx-3"><i class="fab fa-facebook fa-2x text-white"></i></a>
            <a href="" class="mx-3"><i class="fab fa-instagram fa-2x text-white"></i></a>
        </div>
    </div>
    <div class="w-100 d-none d-lg-flex px-5 py-3 align-items-center no-gutters" id="navbar">
        <div class="col-3">
            <form class="form-inline px-3" autocomplete="off">
                <div class="d-flex flex-column">

                    <input class="form-control mr-sm-2 rounded-pill border-0 shadow" type="search" placeholder="Cerca un prodotto" aria-label="Search" id="search-input" onkeyup="search(this.value)">
                    <div id="risultato-ricerca" class="d-none shadow bg-light py-3 px-3 mt-2"></div>
                </div>
            </form>
        </div>
        <div class="col-6 text-center">
            <a href="{{route('home')}}">
                <img src="/images/Logo.png" alt="" class="img-fluid" id="logokaylab">
            </a>
        </div>
        <div class="col-3">
            <div class="text-right">
                <a href="{{route('user', ['tab' => 'carrello'])}}" class="mx-2" id="#favourite-navlink"><img src="/images/icons/like.png" alt="" class="nav-icon"></a>
                <span class="mx-2" id="user-navlink" onclick="usermenu();">
                    <img src="/images/icons/user.png" alt="" class="nav-icon">
                </span>
                <div class="d-none bg-kaylab-light shadow-lg px-4 py-3 mt-2" id="user-menu" style="height: 0px; overflow: hidden;">
                    @guest
                    <h6 class="font-weight-semi-bold text-kaylab-a">Accedi alla tua Area Riservata</h6>
                    <form class="pr-4 pt-2" method="POST" action="{{ route('login') }}">
                        @csrf    
                        <input id="email" type="email" placeholder="E-Mail" class="mb-3 py-2 border-0 shadow-sm form-control form-control-2 dark-text @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
    
                        @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
    
                        <input id="password" type="password" placeholder="Password" class="mb-3 py-2 border-0 shadow-sm form-control form-control-2 dark-text @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">
    
                        @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
    
                        <div class="ml-2 form-check">
                            <input class="form-check-input ml-1" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
    
                            <p class="dark-text mb-0">
                                Ricordami
                            </p>
                        </div>
    
                        <button type="submit" class="btn btn-primary text-white mt-2 mt-lg-3 px-5">
                            {{ __('Login') }}
                        </button>
    
                        @if (Route::has('password.request'))
                            <a class="btn btn-link d-block py-0 text-left text-kaylab-a mt-0 mt-lg-2 px-0" href="{{ route('password.request') }}">
                                Hai dimenticato la tua password?
                            </a>
                        @endif
                        <a href="{{url('/loginregister')}}" class="btn btn-link py-0 text-right text-kaylab-a mt-0 px-0">Se sei un nuovo utente clicka qui per registrarti</a>
                    </form>
                    @else
                    <div class="">
                        @if(Auth::user()->email == 'admin@admin.it' || Auth::user()->email == 'marco93.mv@hotmail.it')
                        <a href="{{route('admin')}}" class="user-link my-2"><p class="text-kaylab-d font-weight-medium m-0">Pannello di controllo</p></a>
                        @endif
                        <a href="{{route('user', ['tab' => 'profilo'])}}" class="user-link my-2">
                            <p class="text-kaylab-d font-weight-medium m-0">Profilo</p>
                        </a>
                        <a href="{{route('user', ['tab' => 'carrello'])}}" class="user-link my-2">
                            <p class="text-kaylab-d font-weight-medium m-0">Carrello</p>
                        </a>
                        <a href="{{route('user', ['tab' => 'ordini'])}}" class="user-link my-2">
                            <p class="text-kaylab-d font-weight-medium m-0">I miei ordini</p>
                        </a>
                        <a href="{{route('user', ['tab' => 'preferiti'])}}" class="user-link my-2">
                            <p class="text-kaylab-d font-weight-medium m-0">Preferiti</p>
                        </a>
                        <a class="user-link my-2" href="{{ route('logout') }}"
                        onclick="event.preventDefault();
                        document.getElementById('logout-form').submit();">
                        <p class="text-kaylab-d font-weight-medium m-0">
                            {{ __('Logout') }}
                        </p>
                        </a>
    
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </div>
                    @endguest
                </div>
                @auth
                <a class="mx-2" onclick="showcart()" id="cartMenuToggler">
                    <img src="/images/icons/carrello.png" alt="" class="nav-icon">                    
                </a>
                <span id="itemsInCart" class="font-weight-semi-bold bg-kaylab-e text-primary <?php if(!Auth::user()->cartelements() > 0){echo('d-none');} ?>">{{Auth::user()->cartelements()}}</span>
                <div class="d-none bg-kaylab-light shadow mt-2 <?php if(Auth::user()->cartelements() > 0){echo('cart-scroll');} ?>" id="cart-menu">
                    @include('layouts.navcart')
                </div>
                @else
                <a href="{{route('loginregister')}}" class="mx-2">
                    <img src="/images/icons/carrello.png" alt="" class="nav-icon">                    
                </a>
                @endauth
            </div>
        </div>
    </div>
</nav>
<div class="d-lg-flex justify-content-center d-none align-items-center bg-white w-100" id="macrocat-nav">
    <a class="" href="{{route('shop', ['macrocat' => 'Neonato'])}}">
        <p class="macrocat-nav-link mx-4 mb-0 font-weight-semi-bold text-kaylab-a" id="macrocat-link-1">Neonato</p>
        <div class="d-none w-100 background-color-kaylab-g" id="macrocat-border-1"></div>
    </a>
    <a class="" href="{{route('shop', ['macrocat' => 'Neonata'])}}">
        <p class="macrocat-nav-link mx-4 mb-0 font-weight-semi-bold text-kaylab-a" id="macrocat-link-2">Neonata</p>
        <div class="d-none w-100 background-color-kaylab-g" id="macrocat-border-2"></div>
    </a>
    <a class="" href="{{route('shop', ['macrocat' => 'Bimbo'])}}">
        <p class="macrocat-nav-link mx-4 mb-0 font-weight-semi-bold text-kaylab-a" id="macrocat-link-3">Bimbo</p>
        <div class="d-none w-100 background-color-kaylab-g" id="macrocat-border-3"></div>
    </a>
    <a class="" href="{{route('shop', ['macrocat' => 'Bimba'])}}">
        <p class="macrocat-nav-link mx-4 mb-0 font-weight-semi-bold text-kaylab-a" id="macrocat-link-4">Bimba</p>
        <div class="d-none w-100 background-color-kaylab-g" id="macrocat-border-4"></div>
    </a>
    <a class="" href="{{route('shop', ['macrocat' => 'MiniMe'])}}">
        <p class="macrocat-nav-link mx-4 mb-0 font-weight-semi-bold text-kaylab-a" id="macrocat-link-5">MiniMe</p>
        <div class="d-none w-100 background-color-kaylab-g" id="macrocat-border-5"></div>
    </a>
    <a class="">
        <p class="macrocat-nav-link mx-4 mb-0 font-weight-semi-bold text-kaylab-a">Chi Siamo</p>
    </a>
    <a class="">
        <p class="macrocat-nav-link mx-4 mb-0 font-weight-semi-bold text-kaylab-a">Faq&Contatti</p>
    </a>
</div>
