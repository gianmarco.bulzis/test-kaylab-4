<div class="spinner-container d-none justify-content-center align-items-center">
    <div class="spinner-border text-primary" role="status">
        <span class="sr-only" id="shop-spinner">Loading...</span>
    </div>
</div>
<div class="row">
    @foreach ($articles as $article)
        <div class="col-6 col-md-4 mb-3 article-item px-1 px-sm-2">
            <a href="{{route('shoparticle', ['id' => $article->id])}}" ondblclick="return false;">
                <div class="shadow article-card bg-white">
                    <div class="article-img m-3 m-xl-4">
                        <img src="{{$article->pictures->first()->getUrl(650,700)}}" alt="{{$article->name}}">
                    </div>
                    <div class="article-info pl-3 pl-xl-4 pr-3 pr-xl-4 pb-3 pb-xl-4">
                        <h4 class="article-title small font-weight-medium text-primary text-truncate">{{$article->name}}</h4>
                        <h6 class="article-price text-primary">{{$article->price}},00€</h6>
                        <p><a>Visualizza <i class="fas fa-arrow-right fa-fw"></i></a></p>
                    </div>
                </div>
            </a>
        </div>
    @endforeach
</div>
{{$articles->links()}}