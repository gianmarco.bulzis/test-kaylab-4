@extends('layouts.app')
@section('content')
<section id="article-detail">
	<div class="container">
		<div class="row">
			<div class="col-3 col-lg-1 col-md-3 order-1 order-lg-1">
				@foreach($article->pictures as $picture)
					<div class="box-gallery">
						<img src="{{ $picture->getUrl(656,840) }}" class="img-fluid" alt="{{ $article->name }}" onclick="swtichimg(this)">
					</div>
				@endforeach
			</div>
			<div class="col-9 col-lg-6 col-md-9 order-2 order-lg-2">
				<div class="box-main-img shadow">
					<img class="card-img-top img-fluid mx-auto" src="{{$article->pictures->first()->getUrl(656,840)}}" alt="{{ $article->name }}">
				</div>
			</div>
			<div class="col-12 col-lg-5 order-3 order-lg-3 mb-lg-0 mb-4">
				<h1 class="text-primary">{{ $article->name }}</h1>
				<h4 class="text-primary">{{ $article->price }},00 €</h4>
				<div class="detail-group d-flex no-gutters my-4">
					<div class="d-flex flex-column">
						<select class="form-control kaylab-select shadow mr-2" id="taglia" name="taglia" onchange="if($(this).val().length > 0){$('#alert-taglia').addClass('d-none');}">
							<option value="">Seleziona una Taglia</option>
							@foreach($sizes as $size)
							<option value="{{$size['id']}}">{{$size['name']}}</option>
							@endforeach
						</select>
						<label for="quantità" class="text-primary text-center font-weight-semi-bold mt-2 d-none" id="alert-taglia"><i class="far fa-exclamation-triangle mr-2"></i>Scegli</label>
					</div>
						<div class="d-flex flex-column">
							<select class="form-control kaylab-select shadow ml-2" id="quantità" name="quantità" onchange="if($(this).val().length > 0){$('#alert-qta').addClass('d-none');}">
								<option value="">Quantità</option>
									<option value="1">1</option>
									<option value="2">2</option>
									<option value="3">3</option>
							</select>
							<label for="quantità" class="text-primary text-center font-weight-semi-bold mt-2 d-none" id="alert-qta"><i class="far fa-exclamation-triangle mr-2"></i>Scegli</label>
						</div>
					</div>
					<div>
						<button class="btn btn-primary shadow" onclick="
							@auth
								addtocart({{$article->id}}, $('#taglia :selected').attr('value'), $('#quantità').val());
							@else
								window.location.href = '/loginregister'
							@endauth
							">Aggiungi al Carrello</button>
					</div>
					<div class="social-group">
						<div>
							<p class="my-3">
								@auth
								<a onclick="favourite({{Auth::user()->id}}, {{$article->id}}, this)">
								<i class="
									@if(Auth::user()->isFavourite($article->id))
									fas
									@else
									fal
									@endif
									fa-heart fa-fw fa-lg text-primary">
								</i>
								@else
								<a onclick="window.location.href = '/loginregister'">
								<i class="fal fa-heart fa-fw fa-lg text-primary"></i>
								@endauth									
								Aggiungi ai Preferiti
								</a>
							</p>
						</div>
						<div>
							<p class="my-2"><a><i class="fal fa-share-alt fa-fw fa-lg"></i> Condividi</a></p>
						</div>
					</div>
					<div class="infos-group pt-3">
						<div class="accordion" id="articleAccordion">
							<div class="card">
								<div class="card-header" id="headingOne">
									<h2 class="mb-0">
										<a class="btn btn-link btn-block text-left" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
										Dettagli Prodotto
										</a>
									</h2>
								</div>							
								<div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#articleAccordion">
									<div class="card-body">
										{{ $article->description }}
									</div>
								</div>
							</div>
							<div class="card">
								<div class="card-header" id="headingTwo">
									<h2 class="mb-0">
										<a class="btn btn-link btn-block text-left collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
										Materiali e Lavaggio
										</a>
									</h2>
								</div>
								<div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#articleAccordion">
									<div class="card-body">
										Some placeholder content for the second accordion panel. This panel is hidden by default.
									</div>
								</div>
							</div>
							<div class="card">
								<div class="card-header" id="headingThree">
									<h2 class="mb-0">
										<a class="btn btn-link btn-block text-left collapsed" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
										Spedizioni e Reso
										</a>
									</h2>
								</div>
								<div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#articleAccordion">
									<div class="card-body">
										And lastly, the placeholder content for the third and final accordion panel. This panel is hidden by default.
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>    
	</div>
</section>
@endsection