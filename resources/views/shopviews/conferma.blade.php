/** CONFERMA.BLADE.PHP **/
@extends('layouts.app')

@section('content')
<?php 
    $total = 0;
    foreach(Auth::user()->carts as $cart) {
        $total += $cart->total();
    }
?>
<section id="riepilogoSection">
    <div class="container">
        <div class="row">
            <div class="col-12 col-lg-8">
                <div class="element-container confirm">
                    <div class="infos">
                        <h4>1. Le mie informazioni</h4> 
                    </div>
                    <div class="grid-infos box-article mt-3">
                        <div>
                            <p>Nome:</p>
                            <p>{!! Auth::user()->name ? Auth::user()->name : '<a href="/profilo">Vai alle impostazioni</a>' !!}</p>
                        </div>
                        <div>
                            <p>Cognome:</p>
                            <p>{!! Auth::user()->cognome ? Auth::user()->cognome : '<a href="/profilo">Vai alle impostazioni</a>' !!}</p>
                        </div>
                        <div>
                            <p>Email:</p>
                            <p>{!! Auth::user()->email ? Auth::user()->email : '<a href="/profilo">Vai alle impostazioni</a>' !!}</p>
                        </div>
                        <div>
                            <p>Telefono:</p>
                            <p>{!! Auth::user()->telefono ? Auth::user()->telefono : '<a href="/profilo">Vai alle impostazioni</a>' !!}</p>
                        </div>
                    </div>
                    <div class="infos">
                        <h4>2. Indirizzo di Consegna</h4> 
                    </div>
                    <div class="box-article">
                        <div>
                            <div>
                               <h5>Indirizzo di spedizione</h5>
                                <div class="align-items-center d-flex justify-content-between mt-4">
                                    <p class="name">Giulia Pignataro</p>
                                    <p>
                                        <a href="#">Modifica</a>
                                    </p>
                                </div>
                                <div class="mt-3">
                                    <p>Via delle Ciole 80125 Napoli</p>
                                    <label for="address" class="d-flex">
                                        <input class="custom-check" type="checkbox" aria-label="Spedisci a questo indirizzo">
                                        <div class="mt-3"> <p>Spedisci a questo indirizzo</p></div>
                                    </label>
                                </div>
                                <div class="mt-4">
                                    <a href="#" class="btn btn-primary rounded-pill">Aggiungi Indirizzo</a>
                                </div>
                            </div>
                            <div class="mt-md-5 mt-3">
                               <h5>Metodo di consegna</h5>
                                <div class="mt-4">
                                    <p class="delivery-tipe">Consegna prevista entro <span class="text-primary"> mercoledì 8 settembre</span></p>
                                </div>
                                <div class="mt-2">
                                    <label for="address" class="d-flex">
                                        <input class="custom-radio" type="radio" name="deliveryValue" value="delivery-1" checked>
                                        <div class="mt-3"> <p>Consegna Standard 6€</p></div>
                                    </label>
                                    <label for="address" class="d-flex">
                                        <input class="custom-radio" type="radio" name="deliveryValue" value="delivery-2">
                                        <div class="mt-3"> <p>Consegna su Appuntamento 20€</p></div>
                                    </label>
                                </div>
                                <div class="mt-4">
                                    <a href="#" class="btn btn-primary rounded-pill">Salva e Continua</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="infos">
                        <h4>3. Pagamento</h4> 
                    </div>
                </div>
            </div>
            <div class="col-12 col-lg-4">
                <div class="element-container recap">
                    <div>    
                       <h4 class="confirm-title">Riepilogo Ordine</h4>
                    </div>
                    <div class="box-price">
                        <div class="prices">
                            <p>Subtotale</p>
                            <p>€ 45</p>
                        </div>
                        <div class="prices">
                            <p>Costi di Spedizione</p>
                            <p>€ 3</p>
                        </div>
                        <div class="prices">
                            <p>Totale Sconti</p>
                            <p>€ 56</p>
                        </div>
                        <div class="prices">
                            <h4>Totale</h4>
                            <h4>
                               € {{ $total }}
                            </h4>
                        </div>
                        <a href="{{ route('conferma') }}" class="btn btn-secondary">Procedi al Pagamento</a>
                    </div>
                    <div class="box-discount">
                        <p>
                            Codice Sconto
                        </p>
                        <div class="form-group">
                            <input type="text" class="form-control" id="discount" name="discount" placeholder="Codice Sconto">
                            <a href="" class="btn btn-secondary">Conferma</a>
                        </div>
                    </div>
                    <div class="box-delivery-info">
                        <div class="info">
                            <i class="fal fa-fw fa-truck"></i>
                            <p>
                                <span>Spedizione Gratuita</span> a partire da 65€
                            </p>
                        </div>
                        <div class="info">
                            <i class="fal fa-fw fa-sync-alt"></i>
                            <p>
                                <span>Reso</span> entro 14 giorni
                            </p>
                        </div>
                        <div class="info">
                            <i class="fal fa-fw fa-house"></i>
                            <p>
                                <span>Acquisto Sicuro</span>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    

</section>
    <a href="{{ route('pagamento') }}" class="btn btn-primary rounded-pill">Paga</a>
@endsection




