@extends('layouts.app')
@section('content')
    <div class="container-fluid" id="shop-main-container">
        <div class="row my-4 align-items-center">
            <div class="col-lg-3">
               <div class="breadcrumb-shop" id="shop-path"> <a href="/">Home</a> / <a href="{{ route('shop') }}">Shop</a> @if(isset($macrocat))/ <a id="pageurlmacrocat">{{$macrocat}}</a>@endif</div>
            </div>
            <div class="col-lg-6 my-3 my-lg-0 text-center">
                <h1>Pantaloni per <i>Bimbo</i></h1>
            </div>
            <div class="col-lg-3">
            </div>
        </div>
    </div>
    <section class="container-fluid position-relative" id="shop-articles-section">
        
        <div class="grid-filters py-4">
            <div class="d-flex">
                <select name="" id="macrocatfilter" class="rounded-pill border-0 form-control shadow-sm btn-outline-kaylab-dark mx-2" onchange="shopfilter()">
                    <option value="null" selected disabled hidden>Taglia</option>
                    <option value="0">Tutti</option>
                    @foreach($macrocats as $taglia)
                    <option value="{{$taglia->id}}" 
                        <?php if(isset($macrocat) && $taglia->name == $macrocat)
                            echo 'selected';
                        ?>
                    >{{$taglia->name}}</option>
                    @endforeach
                </select>
            </div>
            <div class="d-flex">
                <select name="" id="catfilter" class="rounded-pill border-0 form-control shadow-sm btn-outline-kaylab-dark mx-2" onchange="shopfilter()">
                    <option value="null" selected disabled hidden>Tipo di Prodotto</option>
                    <option value="0">Tutti</option>
                    @foreach ($categories as $category)
                    <option value="{{$category->id}}">{{$category->name}}</option>
                    @endforeach
                </select>
            </div>
            <div class="d-flex">
                <select name="" id="collectionfilter" class="rounded-pill border-0 form-control shadow-sm btn-outline-kaylab-dark mx-2" onchange="shopfilter()">
                    <option value="null" selected disabled hidden>Collezione</option>
                    <option value="0">Tutti</option>
                    @foreach ($collezioni as $collezione)
                    <option value="{{$collezione->id}}"
                        <?php if(isset($collection) && $collezione->id == $collection){
                            echo 'selected';
                        } ?>
                    >{{$collezione->name}}</option>
                    @endforeach
                </select>
            </div>
            <div class="d-flex ml-sm-auto">
                <select name="" id="sortfilter" class="rounded-pill border-0 form-control shadow-sm btn-outline-kaylab-dark mx-2" onchange="shopfilter()">
                    <option value="null" selected disabled hidden>Ordina per</option>
                    <option value="1">Prezzo Crescente</option>
                    <option value="2">Prezzo Decrescente</option>
                    <option value="3">Ultimi Aggiunti</option>
                </select>
            </div>
        </div>
        <div id="articles-list">
            <div class="spinner-container d-none justify-content-center align-items-center">
                <div class="spinner-border text-primary" role="status">
                    <span class="sr-only" id="shop-spinner">Loading...</span>
                </div>
            </div>
            <div class="row">
                @foreach ($articoli as $article)
                    <div class="col-6 col-md-4 mb-3 article-item px-1 px-sm-2">
                        <a href="{{route('shoparticle', ['id' => base64_encode($article->id . '_' . $article->name)])}}" ondblclick="return false;">
                            <div class="shadow article-card bg-white">
                                <div class="article-img m-3 m-xl-4">
                                    <img src="{{$article->pictures->first()->getUrl(340,340)}}" alt="{{$article->name}}">
                                </div>
                                <div class="article-info pl-3 pl-xl-4 pr-3 pr-xl-4 pb-3 pb-xl-4">
                                    <h4 class="article-title small font-weight-medium text-primary text-truncate">{{$article->name}}</h4>
                                    <h6 class="article-price text-primary">{{$article->price}},00€</h6>
                                    <p><a>Visualizza <i class="fas fa-arrow-right fa-fw"></i></a></p>
                                </div>
                            </div>
                        </a>
                    </div>
                @endforeach
            </div>
            {{$articoli->links()}}
        </div>
    </section>
@endsection