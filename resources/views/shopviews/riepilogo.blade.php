@extends('layouts.app')

@section('content')
<?php 
    $total = 0;
    foreach(Auth::user()->carts as $cart) {
        $total += $cart->total();
    }
?>
<section class="riepilogoSection" id="riepilogoSection">
    <div class="container">
        <div class="row">
            <div class="col-12 col-lg-8">
                <div class="element-container">
                    <div class="title">
                        <h1>Carrello</h1> 
                        <h6>
                            {{ count($cartElements) }} @if(count($cartElements) > 1) prodotti @else prodotto @endif
                        </h6> 
                    </div>
                    @foreach($cartElements as $element)
                    <div class="box-article">
                        <div class="box-img">
                            <div class="image" style="background-image: url({{ $element->article->pictures->first()->getUrl(90,120) }})"></div>
                        </div>
                        <div class="box-info"> 
                            <h5>{{ $element->article->name }}</h5>
                            <h6>{{ $element->article->price }},00 €</h6>
                            <div class="d-flex align-items-center">
                                <p class="mr-3">{{ $element->size->name }}</p>      
                                <select name="quantity" id="quantity" value="{{ $element->qta }}">
                                    @if($element->article->getMaxStock($element->size->id) < 10) 
                                        @for($i = 1; $i <= $element->article->getMaxStock($element->size->id); $i++)
                                            <option value="{{ $i }}" @if($i == $element->qta) selected @endif> {{ $i }} </option>
                                        @endfor
                                    @else
                                        @for($i = 1; $i <= 10; $i++)
                                            <option value="{{ $i }}" @if($i == $element->qta) selected @endif> {{ $i }} </option>
                                        @endfor
                                    @endif
                                </select>
                            </div>
                            <div class="box-actions">
                                @auth
								<a onclick="favourite({{Auth::user()->id}}, {{$element->article->id}}, this)">
								<i class="
									@if(Auth::user()->isFavourite($element->article->id))
									fas
									@else
									fal
									@endif
									fa-heart fa-fw fa-lg text-primary">
								</i>
								@else
								<a onclick="window.location.href = '/loginregister'">
								<i class="fal fa-heart fa-fw fa-lg text-primary"></i>
								@endauth									
									Aggiungi ai Preferiti
								</a>
                                <a href="">
                                    <i class="fas fa-times"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                    @endforeach
                    <div class="element-detail">
                        <h4>
                            Consegna prevista entro <span>"Data da Destinarsi"</span>
                        </h4>
                    </div>
                </div>
            </div>
            <div class="col-12 col-lg-4">
                <div class="element-container recap">
                    <div class="box-dialog">    
                        <p>Consegna gratuita con "Testo preimpostato"</p>
                        <a href="/shop">
                            <h4>Continua Lo Shopping</h4>
                        </a>
                    </div>
                    <div class="box-price">
                        <div class="prices">
                            <p>Subtotale</p>
                            <p>€ 45</p>
                        </div>
                        <div class="prices">
                            <p>Costi di Spedizione</p>
                            <p>€ 3</p>
                        </div>
                        <div class="prices">
                            <p>Totale Sconti</p>
                            <p>€ 56</p>
                        </div>
                        <div class="prices">
                            <h4>Totale</h4>
                            <h4>
                               € {{ $total }}
                            </h4>
                        </div>
                        <a href="{{ route('conferma') }}" class="btn btn-secondary">Procedi al Pagamento</a>
                    </div>
                    <div class="box-discount">
                        <p>
                            Codice Sconto
                        </p>
                        <div class="form-group">
                            <input type="text" class="form-control" id="discount" name="discount" placeholder="Codice Sconto">
                            <a href="" class="btn btn-secondary">Conferma</a>
                        </div>
                    </div>
                    <div class="box-delivery-info">
                        <div class="info">
                            <i class="fal fa-fw fa-truck"></i>
                            <p>
                                <span>Spedizione Gratuita</span> a partire da 65€
                            </p>
                        </div>
                        <div class="info">
                            <i class="fal fa-fw fa-sync-alt"></i>
                            <p>
                                <span>Reso</span> entro 14 giorni
                            </p>
                        </div>
                        <div class="info">
                            <i class="fal fa-fw fa-house"></i>
                            <p>
                                <span>Acquisto Sicuro</span>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
