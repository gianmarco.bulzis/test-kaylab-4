@extends('layouts.app')

@section('content')
<section class="profile-section">
    <div class="container">
        <div class="row">
            <div class="col-12 col-lg-3">
                <div class="box-tabs shadow mt-4">
                    <div class="px-3 pb-3 pt-0">
                        <h2>
                            My KyLab
                        </h2>
                        <p>
                            Benvenuto/a nella tua area personale
                        </p>
                    </div>
                    <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                        <a class="@if($tab == 'profilo') active @endif" id="v-pills-profilo-tab" data-toggle="pill" href="#v-pills-profilo" role="tab" aria-controls="v-pills-profilo" aria-selected="true">
                            Profilo
                        </a>
                        <a class="@if($tab == 'carrello') active @endif" id="v-pills-carrello-tab" data-toggle="pill" href="#v-pills-carrello" role="tab" aria-controls="v-pills-carrello" aria-selected="false">
                            Carrello
                        </a>
                        <a class="@if($tab == 'preferiti') active @endif" id="v-pills-carrello-tab" data-toggle="pill" href="#v-pills-preferiti" role="tab" aria-controls="v-pills-carrello" aria-selected="false">
                            Preferiti
                        </a>
                        <a class="@if($tab == 'ordini') active @endif" id="v-pills-ordini-tab" data-toggle="pill" href="#v-pills-ordini" role="tab" aria-controls="v-pills-ordini" aria-selected="false">
                            Ordini
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-12 col-lg-9 mt-4 mt-lg-0">
                <div class="tab-content" id="v-pills-tabContent">
                    <div class="tab-pane fade @if($tab=='profilo') show active @endif" id="v-pills-profilo" role="tabpanel" aria-labelledby="v-pills-profilo-tab">
                        @include('user.userTabs.infos')
                    </div>
                    <div class="tab-pane fade @if($tab=='carrello') show active @endif" id="v-pills-carrello" role="tabpanel" aria-labelledby="v-pills-carrello-tab">
                        @include('user.userTabs.cart')
                    </div>
                    <div class="tab-pane fade @if($tab=='preferiti') show active @endif" id="v-pills-preferiti" role="tabpanel" aria-labelledby="v-pills-carrello-tab">
                        @include('user.userTabs.preferiti')
                    </div>
                    <div class="tab-pane fade @if($tab=='ordini') show active @endif" id="v-pills-ordini" role="tabpanel" aria-labelledby="v-pills-ordini-tab">
                        @include('user.userTabs.orders')
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection

{{-- @extends('layouts.app')
DIOPOVERO BOOTSTRAP LO FA IN AUTOMATICO
@section('content')

<div class="w-100 d-flex justify-content-center py-2">
    <a class="mx-2 text-kaylab-d user-tablink <?php if($tab == 'profilo'){echo 'active';} ?>" onclick="usertab('#profilo-tablink', '#tab-profilo')" id="profilo-tablink"><h5>Profilo</h5></a>
    <p>|</p>
    <a class="mx-2 text-kaylab-d user-tablink <?php if($tab == 'carrello'){echo 'active';} ?>" onclick="usertab('#carrello-tablink', '#tab-carrello')" id="carrello-tablink"><h5>Carrello</h5></a>
    <p>|</p>
    <a class="mx-2 text-kaylab-d user-tablink <?php if($tab == 'ordini'){echo 'active';} ?>" onclick="usertab('#ordini-tablink', '#tab-ordini')" id="ordini-tablink"><h5>Ordini</h5></a>
    <p>|</p>
    <a class="mx-2 text-kaylab-d user-tablink <?php if($tab == 'preferiti'){echo 'active';} ?>" onclick="usertab('#preferiti-tablink', '#tab-preferiti')" id="preferiti-tablink"><h5>Preferiti</h5></a>
</div>
<div class="mt-3 border py-3 user-tab <?php if($tab == 'profilo'){echo 'active';}else{echo 'd-none';} ?>" id="tab-profilo">
    <h1>profilo</h1>
</div>
<div class="mt-3 border py-3 user-tab <?php if($tab == 'carrello'){echo 'active';}else{echo 'd-none';} ?>" id="tab-carrello">
    <h1>carrello</h1>

</div>
<div class="mt-3 border py-3 user-tab <?php if($tab == 'ordini'){echo 'active';}else{echo 'd-none';} ?>" id="tab-ordini">
    <h1>ordini</h1>

</div>
<div class="mt-3 border py-3 user-tab <?php if($tab == 'preferiti'){echo 'active';}else{echo 'd-none';} ?>" id="tab-preferiti">
    <h1>preferiti</h1>

</div>
@endsection --}}