<?php 
    $total = 0;
    foreach(Auth::user()->carts as $cart) {
        $total += $cart->total();
    }
?>
<section class="riepilogoSection pt-0" id="riepilogoSection">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="element-container border-0 shadow mt-4">
                    <div class="title">
                        <h2>Carrello</h2> 
                        <h6 class="ml-2">
                            {{ count($cartElements) }} @if(count($cartElements) > 1) prodotti @else prodotto @endif
                        </h6> 
                    </div>
                    @foreach($cartElements as $element)
                    <div class="box-article">
                        <div class="box-img">
                            <div class="image" style="background-image: url({{ $element->article->pictures->first()->getUrl(90,120) }})"></div>
                        </div>
                        <div class="box-info"> 
                            <h5>{{ $element->article->name }}</h5>
                            <h6>{{ $element->article->price }},00 €</h6>
                            <div class="d-flex align-items-center">
                                <p class="mr-3">{{ $element->size->name }}</p>
                                <select name="quantity" id="quantity" data-cart_id="{{$element->id}}" value="{{ $element->qta }}" onchange="changeElementQta({{$element->id}});">
                                    @if($element->article->getMaxStock($element->size->id) < 10) 
                                        @for($i = 1; $i <= $element->article->getMaxStock($element->size->id); $i++)
                                            <option value="{{ $i }}" @if($i == $element->qta) selected @endif> {{ $i }} </option>
                                        @endfor
                                    @else
                                        @for($i = 1; $i <= 10; $i++)
                                            <option value="{{ $i }}" @if($i == $element->qta) selected @endif> {{ $i }} </option>
                                        @endfor
                                    @endif
                                </select>
                            </div>
                            <div class="box-actions">
                                @auth
								<a onclick="favourite({{Auth::user()->id}}, {{$element->article->id}}, this)">
								<i class="
									@if(Auth::user()->isFavourite($element->article->id))
									fas
									@else
									fal
									@endif
									fa-heart fa-fw fa-lg text-primary">
								</i>
								@else
								<a onclick="window.location.href = '/loginregister'">
								<i class="fal fa-heart fa-fw fa-lg text-primary"></i>
								@endauth									
									Aggiungi ai Preferiti
								</a>
                                <a href="">
                                    <i class="fas fa-times"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                    @endforeach
                    <div class="element-detail">
                        <h4>
                            Consegna prevista entro <span>"Data da Destinarsi"</span>
                        </h4>
                    </div>
                    <div class="d-flex justify-content-end p-3">
                        <div>
                            <h5>Totale: </h5>
                        </div>
                        <button class="btn btn-primary">Procedi all'acquisto</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>