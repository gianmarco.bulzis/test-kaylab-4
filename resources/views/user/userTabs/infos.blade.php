<div class="profile-form shadow mt-4 bg-white">
    <h2>Dati Personali</h4>
    <div class="row">
        <div class="col-12 col-lg-6">
            <div class="form-group">
                <label for="name">Nome</label>
                <input class="form-control" type="text" id="name" name="name" value="<?php if(isset(Auth::user()->name)){echo Auth::user()->name;} ?>">
            </div>
        </div>
        <div class="col-12 col-lg-6">            
            <div class="form-group">
                <label for="surname">Cognome</label>
                <input class="form-control" type="text" id="surname" name="surname" value="<?php if(isset(Auth::user()->cognome)){echo Auth::user()->cognome;} ?>">
            </div>
        </div>
        <div class="col-12 col-lg-6">
            <div class="form-group">
                <label for="sex">Sesso</label>
                <select class="form-control" type="text" id="sex" name="sex">
                    <option value=""></option>
                    <option value="M" <?php if(isset(Auth::user()->sesso)){if(Auth::user()->sesso == 'M'){echo 'selected';}} ?>>Maschio</option>
                    <option value="F" <?php if(isset(Auth::user()->sesso)){if(Auth::user()->sesso == 'F'){echo 'selected';}} ?>>Femmina</option>
                    <option value="A" <?php if(isset(Auth::user()->sesso)){if(Auth::user()->sesso == 'A'){echo 'selected';}} ?>>Altro</option>
                </select>
            </div>
        </div>
        <div class="col-12 col-lg-6">            
            <div class="form-group">
                <label for="birthdate">Data di Nascita</label>
                <input class="form-control" type="date" id="birthdate" name="birthdate" value="<?php if(isset(Auth::user()->birthdate)){echo Auth::user()->birthdate;} ?>">
            </div>
        </div>
        <div class="col-12 col-lg-6">
            <div class="form-group">
                <label for="phone">Cellulare</label>
                <input class="form-control" type="tel" id="phone" value="<?php if(isset(Auth::user()->telefono)){echo Auth::user()->telefono;} ?>" name="phone">
            </div>
        </div>
        <div class="col-12 col-lg-6">            
            <div class="form-group">
                <label for="email">Email</label>
                <input class="form-control" disabled type="email" value="{{Auth::user()->email}}" id="email" name="email">
            </div>
        </div>
        <div class="col-12 text-right">
            <a class="btn btn-success rounded-pill" id="save-profile" onclick="saveUserProfile()">salva</a>
        </div>
    </div>
</div>

<div class="profile-form shadow mt-4 bg-white">
    <h4>Cambio Password</h4>
    <div class="row">
        <div class="col-12 col-lg-6">
            <div class="form-group">
                <label for="password">Password</label>
                <input class="form-control" type="password" id="password" name="password">
            </div>
        </div>
        <div class="col-12 col-lg-6">            
            <div class="form-group">
                <label for="confirm_password">Conferma Password</label>
                <input class="form-control" onkeyup="checkPass()"  type="password" name="confirm_password" id="confirm_password">
                <span id='message'></span>
                </label>
            </div>
        </div>
        <div class="col-12 text-right">
            <button class="btn btn-success disabled" id="submitPassButton">Change</button>
        </div>
    </div>
</div>