<div class="container">
    <div class="row">
        <div class="col-12">
            <div id="favourite-container">
                @foreach (Auth::user()->favourites as $favourite)
                <div class="col-6 col-md-4 mb-3 article-card">
                    <a href="{{route('shoparticle', ['id' => $favourite->article->id])}}">
                        <div class="shadow article-card">
                            <div class="article-img m-3 m-xl-4">
                                <img src="{{$favourite->article->pictures->first()->getUrl(650,700)}}" class="img-fluid" alt="{{$favourite->article->name}}">
                            </div>
                            <div class="article-info pl-4 pr-4 pb-4">
                                <h4 class="article-title text-primary">{{$favourite->article->name}}</h4>
                                <h6 class="article-price text-primary">{{$favourite->article->price}},00€</h6>
                                <p><a>Visualizza <i class="fas fa-arrow-right fa-fw"></i></a></p>
                            </div>
                        </div>
                    </a>
                </div>
                @endforeach
            </div>
        </div>
    </div>
</div>