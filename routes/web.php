<?php

use App\Models\Article;
use App\Models\Category;
use App\Models\Collection;
use App\Models\Macrocategory;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\ShopController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\PublicController;
use App\Http\Controllers\NewsletterController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function(){
//     $categories = Category::all();
//     $macrocats = Macrocategory::all();
//     $articoli = Article::all();
//     $collezioni = Collection::all();
//     return redirect('/home');
// });

Auth::routes(['verify' => true]);
Auth::routes();

// Route::get('/', [HomeController::class, 'index'])->name('home');
Route::get('/', [PublicController::class, 'index'])->name('home');
Route::get('/admin', [AdminController::class, 'index'])->name('admin');
Route::get('/admin/articoli', [AdminController::class, 'update'])->name('update');
Route::post('/admin/articoli/insert', [AdminController::class, 'insert'])->name('insert');
Route::post('/admin/collection/insert', [AdminController::class, 'addcollection'])->name('addcollection');

/* Public Routes */

Route::get('/loginregister', [PublicController::class, 'loginregister'])->name('loginregister');

Route::post('/admin/articoli/immagini/upload', [AdminController::class, 'upload'])->name('immagini');
Route::get('/admin/articoli/immagini/reload', [AdminController::class, 'reload'])->name('reload');
Route::delete('/admin/articoli/immagini/remove', [AdminController::class, 'remove']);
Route::get('/admin/articoli/{macroid}/{catid}', [AdminController::class, 'filter']);
Route::get('/admin/articoli/tutti/{prova}', [AdminController::class, 'all']);
Route::get('/articolo/{id}', [AdminController::class, 'showarticle']);
Route::post('/admin/articoli/delete/{id}', [AdminController::class, 'deletearticle']);
Route::post('/admin/articoli/modifica/{id}', [AdminController::class, 'modifica']);
Route::post('/admin/articoli/removeimg/{pid}/{aid}', [AdminController::class, 'removeimg']);
Route::post('/admin/articoli/addsize/{id}/{sid}/{qta}', [AdminController::class, 'addsize']);
Route::get('/home/search', [PublicController::class, 'search']);
Route::get('/article', [PublicController::class, 'showarticle'])->name('shoparticle');
Route::get('/shop', [PublicController::class, 'shop'])->name('shop');
Route::post('/carrello/add/{aid}/{sizeid}/{qta}', [UserController::class, 'addtocart']);
Route::post('/carrello/changeqta/{op}/{cid}', [UserController::class, 'changeqta']);
Route::post('/carrello/remove/{cid}', [UserController::class, 'removefromcart']);

Route::get('newsletter',[NewsletterController::class, 'create'])->name('newsletter');
Route::post('newsletter',[NewsletterController::class, 'store'])->name('newsletter.post');
Route::post('/newsletter/{email}', [NewsletterController::class, 'store']);

// Shop Routes
Route::get('/riepilogo', [ShopController::class, 'riepilogo'])->name('riepilogo');
Route::get('/conferma', [ShopController::class, 'confermaordine'])->name('conferma');
Route::get('/pagamento', [ShopController::class, 'pagamento'])->name('pagamento');
Route::get('/shopfilter', [PublicController::class, 'filter'])->name('shopfilter');


// User Routes
Route::post('/favourite', [UserController::class, 'favourite'])->name('favourite');
Route::get('/my-kaylab', [UserController::class, 'userprofile'])->name('user');
Route::post('/profilo/savedata', [UserController::class, 'saveprofile'])->name('saveprofile');
Route::post('/cartelementchangeqta', [UserController::class, 'cartelementchangeqta'])->name('cartelementchangeqta');



// User Controller Routes

// Route::get('/user/addtocart/{aid}/{uid}', [App\Http\Controllers\UserController::class, 'addtocart']);
Route::fallback(function () {

    return view("home");

});