<?php

namespace App\Http\Controllers;

use App\Models\Cart;
use App\Models\Size;
use App\Models\User;
use App\Models\Article;
use App\Models\Picture;
use App\Models\Category;
use App\Models\Collection;
use App\Models\Articlesize;
use Illuminate\Http\Request;
use App\Models\Macrocategory;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;

class PublicController extends Controller
{
    public function index(){
        $categories = Category::all();
        $macrocats = Macrocategory::all();
        $articoli = Article::all()->take(5);
        $collezioni = Collection::all();
        $nuovi = Article::orderBy('updated_at', 'desc')->take(10)->get();

        return view('home', compact('macrocats', 'categories', 'articoli', 'collezioni', 'nuovi'));
    }

    // Funzione per mostrare pagina articoli e filtri
    public function shop(Request $request){
        $categories = Category::all();
        $macrocats = Macrocategory::all();
        $articoli = Article::all()->take(5);
        $collezioni = Collection::all();
        if(isset($request->group)){
            $articoli = Article::where('macrocat', $request->group)->paginate(3);
            $macrocat = $request->group;
            return view('shopviews.shop', compact('articoli', 'macrocat', 'macrocats', 'categories', 'collezioni'));
        }
        if(isset($request->collection)){
            $articoli = Article::where('collection', $request->macrocat)->paginate(3);
            $collection = $request->collection;
            return view('shopviews.shop', compact('articoli', 'collection', 'macrocats', 'categories', 'collezioni'));
        }
        else{
            $articoli = Article::paginate(12);
            return view('shopviews.shop', compact('articoli', 'categories', 'macrocats', 'collezioni'));
        }
    }

    public function loginregister(){
        return view('auth.loginregister');
    }

    public function search(Request $request){
        $s = $request->s;
        $articles = Article::all();
        $ricerca = [];
        $articoli = [];
        $immagini = [];
        foreach($articles as $article){
            $nome = $article->name;
            if(stripos(strtolower($nome), strtolower($s)) !== false){
                $immagine = Picture::where('article_id', $article->id)->first();
                $url = $immagine->getUrl('650','700');
                array_push($articoli, $article);
                array_push($immagini, $url);
            }
        }
        $cats = Category::all();
        foreach($cats as $cat){
            $catname = $cat->name;
            if(stripos(strtolower($catname), strtolower($s)) !== false){
                $articles = Article::where('cat', $catname)->get();
                foreach($articles as $article){
                    $immagine = Picture::where('article_id', $article->id)->first();
                    $url = $immagine->getUrl('650','700');
                    array_push($articoli, $article);
                    array_push($immagini, $url);
                }
            }
        }
        $ricerca['articoli'] = $articoli;
        $ricerca['immagini'] = $immagini;
        return(json_encode($ricerca));
    }



    // public function showarticle($id){
    //     $article = Article::find($id);
    //     $articlesizes = Articlesize::where('article_id', $id)->get();
    //     $sizes = [];
    //     foreach($articlesizes as $articlesize){
    //         $size = Size::find($articlesize->size_id);
    //         array_push($sizes, $size->name);
    //     }
    //     $allsizes = Size::all();
    //     return view('articleviewmodal', compact('article', 'sizes', 'allsizes'));   
    // }

    public function showarticle(Request $request){
        $id = strtok(base64_decode($request->id), '_');
        $article = Article::findOrFail($id);
     
        $articlesizes = Articlesize::where('article_id', $id)->get();
        $sizes = [];
        foreach($articlesizes as $articlesize){
            $size = Size::find($articlesize->size_id);
            array_push($sizes, ['id'=>$size->id, 'name'=>$size->name]);
        }
        $allsizes = Size::all();
        return view('shopviews.articleview', compact('article', 'sizes', 'allsizes'));
    }

    public function filter(Request $request){
        $articles = Article::where('id', '>', 0);
        // GESTIRE IL VALUE 0 PER RESETTARE I FILTRI
        if(isset($request->macrocatid) && $request->macrocatid > 0){
            $macrocat = Macrocategory::find($request->macrocatid);
            $articles = $articles->where('macrocat', $macrocat->name);
        }
        if(isset($request->catid) && $request->catid > 0){
            $cat = Category::find($request->catid);
            $articles = $articles->where('cat', $cat->name);
        }
        if(isset($request->collectionid) && $request->collectionid > 0){
            $articles = $articles->where('collection', $request->collectionid);
        }
        if(isset($request->sortid) && $request->sortid > 0){
            // id 1 -> prezzo crescente
            // id 2 -> prezzo decrescente
            // id 3 -> ultimi aggiunti
            if($request->sortid == 1){
                $articles = $articles->orderBy('price');
            }
            elseif($request->sortid == 2){
                $articles = $articles->orderByDesc('price');
            }
            elseif($request->sortid == 3){
                $articles = $articles->orderByDesc('updated_at');
            }
        }
        // $articles = $request->collectionid;
        $articles = $articles->paginate(6);
        return view('shopviews.listing', compact('articles'));
    }

}
