<?php

namespace App\Http\Controllers;

use App\Models\Article;
use App\Models\Category;
use App\Models\Collection;
use Illuminate\Http\Request;
use App\Models\Macrocategory;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware(['auth', 'verified']);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $categories = Category::all();
        $macrocats = Macrocategory::all();
        $articoli = Article::all();
        $collezioni = Collection::all();

        return view('home', compact('categories', 'macrocats', 'articoli', 'collezioni'));
    }

}
