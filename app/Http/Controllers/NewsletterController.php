<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Newsletter;

class NewsletterController extends Controller
{
    public function create()
    {
        return view('home');
    }
 
    public function store(Request $request)
    {
        if ( ! Newsletter::isSubscribed($request->email) ) 
        {
            Newsletter::subscribe($request->email);
            return redirect('home')->with('success', 'Grazie per esserti iscritto');
        }
        return redirect('home')->with('error', 'Ci dispiace! Risulti già registrato');
            
    }
}