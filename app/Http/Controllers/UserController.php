<?php

namespace App\Http\Controllers;

use App\Models\Cart;
use App\Models\Size;
use App\Models\User;
use App\Models\Article;
use App\Models\Favourite;
use App\Models\Articlesize;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\PublicController;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function addtocart($aid, $sizeid, $qta){
        $article = Article::find($aid);
        $articlesizes = Articlesize::where('article_id', $aid)->where('size_id');
        $carts = Cart::where('user_id', Auth::user()->id)->where('size_id', $sizeid)->get();
        if(count($carts)>0){
            foreach($carts as $cart){
                if($cart->article_id == $aid){
                    if($cart->size_id == $sizeid){
                       $cart->qta += $qta;
                       $cart->save();
                        return view('layouts.navcart');
                    }
                    else{
                        $cartelement = new Cart();
                        $cartelement->user_id = Auth::user()->id;
                        $cartelement->article_id = $article->id;
                        $cartelement->size_id = $sizeid;
                        $cartelement->qta = $qta;
                        $cartelement->save();
                        return view('layouts.navcart');
                    }
                }else{
                    $cartelement = new Cart();
                    $cartelement->user_id = Auth::user()->id;
                    $cartelement->article_id = $article->id;
                    $cartelement->size_id = $sizeid;
                    $cartelement->qta = $qta;
                    $cartelement->save();
                    return view('layouts.navcart');
                }
            }
        }
        else{
            $cartelement = new Cart();
            $cartelement->user_id = Auth::user()->id;
            $cartelement->article_id = $article->id;
            $cartelement->size_id = $sizeid;
            $cartelement->qta = $qta;
            $cartelement->save();
            return view('layouts.navcart');
        }
        // return Auth::user()->cartelements();
    }



    
    public function changeqta($op, $cid){
        $cart = Cart::find($cid);
        $articlesize = Articlesize::where('article_id', $cart->article_id)->where('size_id', $cart->size_id)->get();
        $messaggio = 'Massima disponibilità';
        if($op == 'sum'){ 
            if($cart->qta < $articlesize[0]['qta']){
                $cart->qta += 1;
                $cart->save();
                return view('layouts.navcart');
            }
            else{
                return view('layouts.navcart', compact('messaggio', 'cid'));
            }
        }
        elseif($op == 'dif'){
            if($cart->qta > 1){
                $cart->qta -= 1;
                $cart->save();
                return view('layouts.navcart');
            }
            else{
                return view('layouts.navcart');
            }
        }
    }

    public function removefromcart($cid){
        Cart::find($cid)->delete();
        return view('layouts.navcart');
    }

    public function userprofile(Request $request){
        $tab = $request->tab;
        $cartElements = Cart::where('user_id', Auth::user()->id)->get();
        return view('user.profile', compact('tab', 'cartElements'));
    }

    public function saveprofile(Request $request){
        $user = User::find(Auth::user()->id);
        $user->name = $request->user['nome'];
        $user->cognome = $request->user['cognome'];
        $user->sesso = $request->user['sesso'];
        $user->birthdate = $request->user['birthdate'];
        $user->telefono = $request->user['cellulare'];
        $user->save();
        return $user->name;
    }

    public function favourite(Request $request){
        if($request->user()->isFavourite($request->articleid)){
            Favourite::where('user_id', Auth::user()->id)->where('article_id', $request->articleid)->delete();
            return 'deleted';
        }
        else{
            $favourite = new Favourite();
            $favourite->user_id = $request->userid;
            $favourite->article_id = $request->articleid;
            $favourite->save();
            return 'added';
        }

    }

    public function cartelementchangeqta(Request $request){
        $cart = Cart::find($request->cartid);
        $cart->qta = $request->qta;
        $cart->save();
        return view('layouts.navcart');
    }
}
