<?php

namespace App\Http\Controllers;

use App\Models\Size;
use App\Models\User;
use App\Models\Article;
use App\Models\Picture;
use App\Models\Category;
use App\Jobs\ResizeImage;
use App\Models\Collection;
use App\Models\Articlesize;
use Illuminate\Http\Request;
use App\Models\Macrocategory;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Storage;


class AdminController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request){
        // $macrocategories = ['Neonato', 'Neonata', 'Bimba', 'Bimbo', 'MiniMe'];
        // $categories = Category::all();
        // // $categories = ['Accessori', /*calzini, accessori capelli, cappelli, sciarpe*/ 'T-Shirt/Magliette',
        // // 'felpe/maglioni', 'gonne', 'pantaloni', 'abiti', 'bluse/camicie', 'giacche/cardigan', 'giubbini/capi spalla' ];
        // return view('home', compact("categories"));

        if(Auth::User()->email == "admin@admin.it"){

            $categories = Category::all();
            $macrocats = Macrocategory::all();
            $collezioni = Collection::all();
            $articoli = Article::orderBy('created_at', 'desc')->get();
            $sizes = Size::all();
            $uniqueSecret = $request->old('uniqueSecret', base_convert(sha1(uniqid(mt_rand())), 16, 36));

            return view('admin.adminhome', compact('categories', 'macrocats', 'collezioni', 'articoli', 'uniqueSecret', 'sizes'));
        }
        else{
            return redirect('/home');
        }
    }

    public function dropzonetest(Request $request){
        dd($request->input());
    }


    public function insert(Request $request){
        $article = new Article();
        if($request->input('macroselect')){
            $article->macrocat = $request->input('macroselect');
        }
        if($request->input('collezione')){
            $article->collection = $request->input('collezione');
        }
        $article->cat = $request->input('catselect');
        $article->name = $request->input('name');
        $article->price = $request->input('price');
        $article->description = $request->input('description');
        // $article->photo = 'storage/'.$this->photo->store('uploads', 'public');
        // $image = Image::make(public_path($article->photo))->fit(250, 250);
        // $image->save();
        $article->save();
        $array = $request->input('size');
        foreach($array as $a){
            switch($a){
                case "xs":
                    $a = 1;
                    $qta = $request->input('qta-xs');
                    break;
                case "s":
                    $a = 2;
                    $qta = $request->input('qta-s');
                    break;
                case "m":
                    $a = 3;
                    $qta = $request->input('qta-m');
                    break;
                case "l":
                    $a = 4;
                    $qta = $request->input('qta-l');
                    break;
                case "xl":
                    $a = 5;
                    $qta = $request->input('qta-xl');
                    break;
                case "xxl":
                    $a = 6;
                    $qta = $request->input('qta-xxl');
                    break;
                case "0/3-mesi":
                    $a = 7;
                    $qta = $request->input('qta-0/3-mesi');
                    break;
                case "3/6-mesi":
                    $a = 8;
                    $qta = $request->input('qta-3/6-mesi');
                    break;
                case "6/9-mesi":
                    $a = 9;
                    $qta = $request->input('qta-6/9-mesi');
                    break;
                case "9/12-mesi":
                    $a = 10;
                    $qta = $request->input('qta-9/12-mesi');
                    break;
                case "12/18-mesi":
                    $a = 11;
                    $qta = $request->input('qta-12/18-mesi');
                    break;
                case "18/24-mesi":
                    $a = 12;
                    $qta = $request->input('qta-18/24-mesi');
                    break;
                case "2/3-anni":
                    $a = 13;
                    $qta = $request->input('qta-2/3-anni');
                    break;
                case "3/4-anni":
                    $a = 14;
                    $qta = $request->input('qta-3/4-anni');
                    break;
                case "4/5-anni":
                    $a = 15;
                    $qta = $request->input('qta-4/5-anni');
                    break;
                case "5/6-anni":
                    $a = 16;
                    $qta = $request->input('qta-5/6-anni');
                    break;
                case "6/7-anni":
                    $a = 17;
                    $qta = $request->input('qta-6/7-anni');
                    break;
                case "7/8-anni":
                    $a = 18;
                    $qta = $request->input('qta-7/8-anni');
                    break;
                case "9/10-anni":
                    $a = 19;
                    $qta = $request->input('qta-9/10-anni');
                    break;
                case "11/12-anni":
                    $a = 20;
                    $qta = $request->input('qta-11/12-anni');
                    break;
                case "0/2-anni":
                    $a = 21;
                    $qta = $request->input('qta-0/2-anni');
                    break;
                case "2/4-anni":
                    $a = 22;
                    $qta = $request->input('qta-2/4-anni');
                    break;
                case "4/6-anni":
                    $a = 23;
                    $qta = $request->input('qta-4/6-anni');
                    break;
            }
            $articlesize = new Articlesize();
            $articlesize->article_id = $article->id;
            $articlesize->size_id = $a;
            $articlesize->qta = $qta;
            $articlesize->save();
        }

        $us = $request->input('uniqueSecret');
        $images = session()->get("images.{$us}", []);
        $removedimages = session()->get("removedimages.{$us}", []);
        $images = array_diff($images, $removedimages);
        foreach($images as $image){
            $p = new Picture();
            $filename = basename($image);
            $file = "public/articoli/{$article->id}/{$filename}";
            Storage::move($image, $file);
            dispatch(new ResizeImage($file, 656, 840));
            dispatch(new ResizeImage($file, 650, 700));
            dispatch(new ResizeImage($file, 340, 340));
            dispatch(new ResizeImage($file, 250, 425));
            dispatch(new ResizeImage($file, 200, 256));
            dispatch(new ResizeImage($file, 90, 120));
            dispatch(new ResizeImage($file, 50, 85));
            $file = "storage/articoli/{$article->id}/{$filename}";
            $p->file = $file;
            $p->article_id = $article->id;
            $p->save();
        }

        File::deleteDirectory(storage_path("app/public/temp/{$us}"));

        $us = base_convert(sha1(uniqid(mt_rand())), 16, 36);

        // session()->flash('message', 'Articolo aggiungo correttamente.');
        return redirect()->to('/admin')->with('message', 'Articolo inserito correttamente.');
    }

    public function addcollection(Request $request){
        $collection = new Collection();
        $request->validate([
            'collectionpicture' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);
        $colpic = $request->collectionpicture;
        $uniqueSecret = $request->input('uniqueSecret');
        $file = $colpic->store("public/temp/{$uniqueSecret}");
        $filename = basename($file);
        Storage::move($file, "public/collezioni/{$filename}");
        File::deleteDirectory(storage_path('app/public/temp'));
        $collection->picture = "storage/collezioni/{$filename}";
        $collection->name = $request->collectionname;
        $collection->description = $request->collectiondescription;
        $collection->save();
        return redirect()->to('/admin')->with('message', 'Collezione aggiunta');
    }

    public function update(){
        $articoli = Article::all()->sortByDesc("created_at");
        return view('articoli', compact('articoli'));
    }

    public function upload(Request $request){
        $uniqueSecret = $request->input('uniqueSecret');
        $filename = $request->file('file')->store("public/temp/{$uniqueSecret}");
        session()->push("images.{$uniqueSecret}", $filename);
        return response()->json(
            [
                'id' => $filename,
            ]
        );
    }

    public function reload(Request $request){
        dd("hello");
        $uniqueSecret = $request->input('uniqueSecret');
        $images = session()->get("images.{$uniqueSecret}", []);
        $removedimages = session()->get("removedimages.{$uniqueSecret}", []);
        $images = array_diff($images, $removedimages);
        $data = [];
        foreach($images as $image){
            $data[] = [
                'id' => $image,
                'src' => Storage::url($image)
            ];
        }
        return response()->json($data);
    }

    public function remove(Request $request){
        $uniqueSecret = $request->input('uniqueSecret');
        $filename = $request->input('id');
        session()->push("removedimages.{$uniqueSecret}", $filename);
        Storage::delete($filename);
        return response()->json('ok');
    }

    public function all($prova){
        $articoli = Article::orderBy('created_at', 'desc')->get();
        return view('articolifiltrati', compact('articoli'));
    }

    public function filter($macroid, $catid){
        if($macroid > 0 && $catid > 0){
            $macrocat = Macrocategory::find($macroid);
            $cat = Category::find($catid);
            $macroname = $macrocat->name;
            $catname = $cat->name;
            $articoli = Article::where('macrocat', $macroname)->where('cat', $catname)->orderBy('created_at', 'desc')->get();
        }
        elseif($macroid > 0 && $catid == 0){
            $macrocat = Macrocategory::find($macroid);
            $macroname = $macrocat->name;
            $articoli = Article::where('macrocat', $macroname)->orderBy('created_at', 'desc')->get();
        }
        elseif($macroid == 0 && $catid > 0){
            $cat = Category::find($catid);
            $catname = $cat->name;
            $articoli = Article::where('cat', $catname)->orderBy('created_at', 'desc')->get();
        }
        else{
            $articoli = Article::orderBy('created_at', 'desc')->get();
        }
        return view('articolifiltrati', compact('articoli'));
    }


    public function showarticle($id){
        $articolo = Article::find($id);
        $sizes = Size::all();
        $articlesizes = Articlesize::where('article_id', $id)->get();
        return view('admin.gestionearticolo', compact('articolo', 'sizes', 'articlesizes'));
    }

    public function deletearticle($id){
        $article = Article::find($id);
        foreach($article->pictures as $picture){
            $picture->delete();
        }
        $array = $article->sizes();
        foreach($array as $size){
            switch($size){
                case 'xs':
                    $articlesize = Articlesize::where('article_id', $id)->where('size_id', 1)->first();
                    $articlesize->delete();
                    break;
                case 's':
                    $articlesize = Articlesize::where('article_id', $id)->where('size_id', 2)->first();
                    $articlesize->delete();
                    break;
                case 'm':
                    $articlesize = Articlesize::where('article_id', $id)->where('size_id', 3)->first();
                    $articlesize->delete();
                    break;
                case 'l':
                    $articlesize = Articlesize::where('article_id', $id)->where('size_id', 4)->first();
                    $articlesize->delete();
                    break;
                case 'xl':
                    $articlesize = Articlesize::where('article_id', $id)->where('size_id', 5)->first();
                    $articlesize->delete();
                    break;
                case "xxl":
                    $articlesize = Articlesize::where('article_id', $id)->where('size_id', 6)->first();
                    $articlesize->delete();
                    break;
                case "0/3-mesi":
                    $articlesize = Articlesize::where('article_id', $id)->where('size_id', 7)->first();
                    $articlesize->delete();
                    break;
                case "3/6-mesi":
                    $articlesize = Articlesize::where('article_id', $id)->where('size_id', 8)->first();
                    $articlesize->delete();
                    break;
                case "6/9-mesi":
                    $articlesize = Articlesize::where('article_id', $id)->where('size_id', 9)->first();
                    $articlesize->delete();
                    break;
                case "9/12-mesi":
                    $articlesize = Articlesize::where('article_id', $id)->where('size_id', 10)->first();
                    $articlesize->delete();
                    break;
                case "12/18-mesi":
                    $articlesize = Articlesize::where('article_id', $id)->where('size_id', 11)->first();
                    $articlesize->delete();
                    break;
                case "18/24-mesi":
                    $articlesize = Articlesize::where('article_id', $id)->where('size_id', 12)->first();
                    $articlesize->delete();
                    break;
                case "2/3-anni":
                    $articlesize = Articlesize::where('article_id', $id)->where('size_id', 13)->first();
                    $articlesize->delete();
                    break;
                case "3/4-anni":
                    $articlesize = Articlesize::where('article_id', $id)->where('size_id', 14)->first();
                    $articlesize->delete();
                    break;
                case "4/5-anni":
                    $articlesize = Articlesize::where('article_id', $id)->where('size_id', 15)->first();
                    $articlesize->delete();
                    break;
                case "5/6-anni":
                    $articlesize = Articlesize::where('article_id', $id)->where('size_id', 16)->first();
                    $articlesize->delete();
                    break;
                case "6/7-anni":
                    $articlesize = Articlesize::where('article_id', $id)->where('size_id', 17)->first();
                    $articlesize->delete();
                    break;
                case "7/8-anni":
                    $articlesize = Articlesize::where('article_id', $id)->where('size_id', 18)->first();
                    $articlesize->delete();
                    break;
                case "9/10-anni":
                    $articlesize = Articlesize::where('article_id', $id)->where('size_id', 19)->first();
                    $articlesize->delete();
                    break;
                case "11/12-anni":
                    $articlesize = Articlesize::where('article_id', $id)->where('size_id', 20)->first();
                    $articlesize->delete();
                    break;
                case "0/2-anni":
                    $articlesize = Articlesize::where('article_id', $id)->where('size_id', 21)->first();
                    $articlesize->delete();
                    break;
                case "2/4-anni":
                    $articlesize = Articlesize::where('article_id', $id)->where('size_id', 22)->first();
                    $articlesize->delete();
                    break;
                case "4/6-anni":
                    $articlesize = Articlesize::where('article_id', $id)->where('size_id', 23)->first();
                    $articlesize->delete();
                    break;
            }
        }
        $article->delete();
        $articoli = Article::all()->sortByDesc("created_at");
        return view('articolifiltrati', compact('articoli'));
    }

    public function modifica(Request $request, $id){
        $article = Article::find($id);
        if($article->name != $request->input('name')){
            $article->name = $request->input('name');
        }
        if($article->price != $request->input('price')){
            $article->price = $request->input('price');
        }
        if($article->description != $request->input('description')){
            $article->description = $request->input('description');
        }
        $article->save();
        $array = $request->input('size');
        foreach($array as $a){
            switch($a){
                case "xs":
                    $a = 1;
                    $qta = $request->input('qta-xs');
                    break;
                case "s":
                    $a = 2;
                    $qta = $request->input('qta-s');
                    break;
                case "m":
                    $a = 3;
                    $qta = $request->input('qta-m');
                    break;
                case "l":
                    $a = 4;
                    $qta = $request->input('qta-l');
                    break;
                case "xl":
                    $a = 5;
                    $qta = $request->input('qta-xl');
                    break;
                case "xxl":
                    $a = 6;
                    $qta = $request->input('qta-xxl');
                    break;
                case "0/3-mesi":
                    $a = 7;
                    $qta = $request->input('qta-0/3-mesi');
                    break;
                case "3/6-mesi":
                    $a = 8;
                    $qta = $request->input('qta-3/6-mesi');
                    break;
                case "6/9-mesi":
                    $a = 9;
                    $qta = $request->input('qta-6/9-mesi');
                    break;
                case "9/12-mesi":
                    $a = 10;
                    $qta = $request->input('qta-9/12-mesi');
                    break;
                case "12/18-mesi":
                    $a = 11;
                    $qta = $request->input('qta-12/18-mesi');
                    break;
                case "18/24-mesi":
                    $a = 12;
                    $qta = $request->input('qta-18/24-mesi');
                    break;
                case "2/3-anni":
                    $a = 13;
                    $qta = $request->input('qta-2/3-anni');
                    break;
                case "3/4-anni":
                    $a = 14;
                    $qta = $request->input('qta-3/4-anni');
                    break;
                case "4/5-anni":
                    $a = 15;
                    $qta = $request->input('qta-4/5-anni');
                    break;
                case "5/6-anni":
                    $a = 16;
                    $qta = $request->input('qta-5/6-anni');
                    break;
                case "6/7-anni":
                    $a = 17;
                    $qta = $request->input('qta-6/7-anni');
                    break;
                case "7/8-anni":
                    $a = 18;
                    $qta = $request->input('qta-7/8-anni');
                    break;
                case "9/10-anni":
                    $a = 19;
                    $qta = $request->input('qta-9/10-anni');
                    break;
                case "11/12-anni":
                    $a = 20;
                    $qta = $request->input('qta-11/12-anni');
                    break;
                case "0/2-anni":
                    $a = 21;
                    $qta = $request->input('qta-0/2-anni');
                    break;
                case "2/4-anni":
                    $a = 22;
                    $qta = $request->input('qta-2/4-anni');
                    break;
                case "4/6-anni":
                    $a = 23;
                    $qta = $request->input('qta-4/6-anni');
                    break;
            }
            if(Articlesize::where('article_id', $id)->where('size_id', $a)->first()){
                $articlesize = Articlesize::where('article_id', $id)->where('size_id', $a)->first();
                if($qta == 0){
                    $articlesize->delete();
                }
                else{
                    $articlesize->qta = $qta;
                    $articlesize->save();
                }
            }
        }
        return redirect()->to('/admin')->with('message', 'Articolo aggiornato correttamente.');
    }

    public function removeimg($pid, $aid){
        $picture = Picture::find($pid);
        $picture->delete();
        $articolo = Article::find($aid);
        $sizes = Size::all();
        $articlesizes = Articlesize::where('article_id', $aid)->get();
        return view('admin.gestionearticolo', compact('articolo', 'sizes', 'articlesizes'));
    }

    public function addsize($id ,$sid, $qta){
        $articolo = Article::find($id);
        if(Articlesize::where('article_id', $id)->where('size_id', $sid)->first()){
            $articlesize = Articlesize::where('article_id', $id)->where('size_id', $sid)->first();
            if($articlesize->qta != $qta){
                $articlesize->qta = $qta;
                $articlesize->save();
            }
        }
        else{
            $articlesize = new Articlesize;
            $articlesize->article_id = $id;
            $articlesize->size_id = $sid;
            $articlesize->qta = $qta;
            $articlesize->save();
        }
        $articlesizes = Articlesize::where('article_id', $id)->get();
        $sizes = Size::all();
        return view('admin.gestionearticolo', compact('articolo', 'sizes', 'articlesizes'));
    }
}
