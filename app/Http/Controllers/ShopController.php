<?php

namespace App\Http\Controllers;

use App\Models\Cart;
use Omnipay\Omnipay;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ShopController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function riepilogo() {
        $cartElements = Cart::where('user_id', Auth::user()->id)->get();
        return view('shopviews.riepilogo', compact('cartElements'));
    }

    public function confermaordine() {
        $cartElements = Cart::where('user_id', Auth::user()->id)->get();
        return view('shopviews.conferma', compact('cartElements'));
    }

    public function pagamento(){
        $cartElements = Cart::where('user_id', Auth::user()->id)->get();
        $totale = 0;
        $prunit = 0;
        foreach($cartElements as $cartElement){
            $totale += $cartElement->total();
        }
        dd($totale);
    }
    
}
