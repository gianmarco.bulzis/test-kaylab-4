<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Collection extends Model
{
    use HasFactory;

    protected $table = "Collections";

    protected $fillable = [
        'name'
    ];

    public function articles(){
        return $this->hasMany(Article::class);
    }
}
