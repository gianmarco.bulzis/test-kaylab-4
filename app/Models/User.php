<?php

namespace App\Models;

use App\Models\Cart;
use App\Models\Article;
use App\Models\Favourite;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable implements MustVerifyEmail
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function cartelements(){
        // $cartelements = Cart::where('user_id', $this->id)->get();
        $qta = 0;
        foreach($this->carts as $element){
            $qta += $element->qta;
        }
        return($qta);
    }

    public function carts(){
        return $this->hasMany(Cart::class);
    }

    public function favourites(){
        return $this->hasMany(Favourite::class);
    }

    public function isFavourite($id){
        $favourite = Favourite::where('user_id', $this->id)->where('article_id', $id)->first();

        return $favourite ? true : false;
    }

}
