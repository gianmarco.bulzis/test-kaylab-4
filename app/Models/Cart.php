<?php

namespace App\Models;

use App\Models\Article;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Cart extends Model
{
    use HasFactory;

    public function article(){
        return $this->belongsTo(Article::class);
    }

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function size(){
        return $this->belongsTo(Size::class);
    }

    public function total(){
        return $this->article->price * $this->qta;
    }
}
