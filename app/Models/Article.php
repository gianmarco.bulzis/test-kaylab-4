<?php

namespace App\Models;

use App\Models\Size;
use App\Models\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Article extends Model
{
    use HasFactory;

    protected $table = "articles";

    protected $fillable = [
        'name', 'macrocat', 'cat', 'price'
    ];

    public function pictures(){
        return $this->hasMany(Picture::class);
    }

    public function sizes(){
        $articlesizes = Articlesize::where('article_id', $this->id)->get();
        $sizes = [];
        foreach($articlesizes as $articlesize){
            $size = Size::find($articlesize->size_id);
            array_push($sizes, $size->name);
        }
        return $sizes;
    }

    public function articlesize(){
        return $this->hasMany(Articlesize::class);
    }

    public function getMaxStock($sizeId) {
        return $this->articlesize()->where('size_id', $sizeId)->first()->qta;
    }

    public function collection(){
        return $this->belongsTo(Collection::class);
    }
}
